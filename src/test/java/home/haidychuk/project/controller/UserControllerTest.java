package home.haidychuk.project.controller;

import static org.hamcrest.Matchers.is;

import static org.junit.Assert.assertEquals;

import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.any;
import static org.mockito.Mockito.anyInt;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import home.haidychuk.project.model.entity.User;
import home.haidychuk.project.model.service.UserService;

import java.util.Arrays;
import java.util.Date;

import org.junit.Test;

import org.mockito.ArgumentCaptor;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.core.MethodParameter;
import org.springframework.http.MediaType;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.MethodArgumentNotValidException;

/**F
 * Test class for UserControllers.
 * @author Andrii Hiadychuk created 10.05.2018
 * @version 1.0
 */
public class UserControllerTest extends ControllerTest {
  // *** Fields *** //

  @MockBean
  private UserService userServiceMock;

  @Autowired
  private UserController userController;

  @Test
  public void testFindById() throws Exception {
    //set up stub
    when(userServiceMock.findById(anyInt())).thenReturn(expectedUser1);

    //checking http status, content type and content in JSON
    mockMvc.perform(get("/user/{id}", 1))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
            .andExpect(jsonPath("$.id", is(1)))
            .andExpect(jsonPath("$.userType", is("USER")))
            .andExpect(jsonPath("$.userStatus", is("ACTIVE")))
            .andExpect(jsonPath("$.firstName", is("Vira")))
            .andExpect(jsonPath("$.secondName", is("Kuchma")))
            .andExpect(jsonPath("$.login", is("vira")))
            .andExpect(jsonPath("$.createdDate", is("2017-08-24 14:28:34")))
            .andExpect(jsonPath("$.closedDate", is((String)null)));

    //verify number of method calling
    verify(userServiceMock, times(1)).findById(anyInt());
    //verify that other method of mocked object were not called
    verifyNoMoreInteractions(userServiceMock);
  }

  @Test
  public void testFindAll() throws Exception {
    //set up stub
    when(userServiceMock.findAll())
            .thenReturn(Arrays.asList(expectedUser1, expectedUser2, expectedUser3));

    //checking http status, content type and content in JSON
    mockMvc.perform(get("/user"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
            //first user
            .andExpect(jsonPath("$[0].id", is(1)))
            .andExpect(jsonPath("$[0].userType", is("USER")))
            .andExpect(jsonPath("$[0].userStatus", is("ACTIVE")))
            .andExpect(jsonPath("$[0].firstName", is("Vira")))
            .andExpect(jsonPath("$[0].secondName", is("Kuchma")))
            .andExpect(jsonPath("$[0].login", is("vira")))
            .andExpect(jsonPath("$[0].createdDate", is("2017-08-24 14:28:34")))
            .andExpect(jsonPath("$[0].closedDate", is((String)null)))
            //second user
            .andExpect(jsonPath("$[1].id", is(2)))
            .andExpect(jsonPath("$[1].userType", is("ADMIN")))
            .andExpect(jsonPath("$[1].userStatus", is("ACTIVE")))
            .andExpect(jsonPath("$[1].firstName", is("Andrii")))
            .andExpect(jsonPath("$[1].secondName", is("Haidychuk")))
            .andExpect(jsonPath("$[1].login", is("andrii")))
            .andExpect(jsonPath("$[1].createdDate", is("2017-07-30 10:16:42")))
            .andExpect(jsonPath("$[1].closedDate", is((String)null)))
            //third user
            .andExpect(jsonPath("$[2].id", is(3)))
            .andExpect(jsonPath("$[2].userType", is("USER")))
            .andExpect(jsonPath("$[2].userStatus", is("CLOSED")))
            .andExpect(jsonPath("$[2].firstName", is("Taras")))
            .andExpect(jsonPath("$[2].secondName", is("Haidychuk")))
            .andExpect(jsonPath("$[2].login", is("taras")))
            .andExpect(jsonPath("$[2].createdDate", is("2017-08-10 06:38:51")))
            .andExpect(jsonPath("$[2].closedDate", is("2017-09-05 13:08:21")));

    //verify number of method calling
    verify(userServiceMock, times(1)).findAll();
    //verify that other method of mocked object were not called
    verifyNoMoreInteractions(userServiceMock);
  }

  @Test
  public void testCreate() throws Exception {
    //create new User
    User newUser = new User(
            User.UserType.USER, User.UserStatus.ACTIVE,
            "TestName", "TestSecondName",
            "test", "testpassword"
    );

    //set up stub
    when(userServiceMock.create(any(User.class))).thenReturn(newUser);
    //checking http status, content type and content in JSON
    mockMvc.perform(post("/user")
            .contentType(MediaType.APPLICATION_JSON_UTF8)
            .content(convertObjectToJson(newUser))
    )
            .andExpect(status().isCreated())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
            //created User
            .andExpect(jsonPath("$.userType", is("USER")))
            .andExpect(jsonPath("$.userStatus", is("ACTIVE")))
            .andExpect(jsonPath("$.firstName", is("TestName")))
            .andExpect(jsonPath("$.secondName", is("TestSecondName")))
            .andExpect(jsonPath("$.login", is("test")));

    ArgumentCaptor<User> captor = ArgumentCaptor.forClass(User.class);

    //verify number of method calling
    verify(userServiceMock, times(1)).create(captor.capture());
    //verify that other method of mocked object were not called
    verifyNoMoreInteractions(userServiceMock);

    assertEquals("TestName", captor.getValue().getFirstName());
    assertEquals("TestSecondName", captor.getValue().getSecondName());
    assertEquals("test", captor.getValue().getLogin());
  }

  @Test
  public void testUpdate() throws Exception {
    expectedUser1.setFirstName("TestName");
    //set up stub
    when(userServiceMock.update(any(User.class))).thenReturn(expectedUser1);

    //checking http status, content type and content in JSON
    mockMvc.perform(put("/user")
            .contentType(MediaType.APPLICATION_JSON_UTF8)
            .content(convertObjectToJson(expectedUser1))
    )
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
            .andExpect(jsonPath("$.id", is(1)))
            .andExpect(jsonPath("$.userType", is("USER")))
            .andExpect(jsonPath("$.userStatus", is("ACTIVE")))
            .andExpect(jsonPath("$.firstName", is("TestName")))
            .andExpect(jsonPath("$.secondName", is("Kuchma")))
            .andExpect(jsonPath("$.createdDate", is("2017-08-24 14:28:34")))
            .andExpect(jsonPath("$.closedDate", is((String)null)));

    ArgumentCaptor<User> captor = ArgumentCaptor.forClass(User.class);

    //verify number of method calling
    verify(userServiceMock, times(1)).update(captor.capture());
    //verify that other method of mocked object were not called
    verifyNoMoreInteractions(userServiceMock);

    assertEquals("TestName", captor.getValue().getFirstName());
    assertEquals("Kuchma", captor.getValue().getSecondName());
    assertEquals("vira", captor.getValue().getLogin());
  }

  @Test
  public void testDelete() throws Exception {
    //set up stub
    when(userServiceMock.delete(anyInt())).thenReturn(expectedUser1);

    //checking http status, content type and content in JSON
    mockMvc.perform(delete("/user/{id}", 1))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
            .andExpect(jsonPath("$.id", is(1)))
            .andExpect(jsonPath("$.userType", is("USER")))
            .andExpect(jsonPath("$.userStatus", is("ACTIVE")))
            .andExpect(jsonPath("$.firstName", is("Vira")))
            .andExpect(jsonPath("$.secondName", is("Kuchma")))
            .andExpect(jsonPath("$.createdDate", is("2017-08-24 14:28:34")))
            .andExpect(jsonPath("$.closedDate", is((String)null)));

    //verify number of method calling
    verify(userServiceMock, times(1)).delete(anyInt());
    //verify that other method of mocked object were not called
    verifyNoMoreInteractions(userServiceMock);
  }

  @Test
  public void testActivate() throws Exception {
    expectedUser3.setUserStatus(User.UserStatus.ACTIVE);
    expectedUser3.setClosedDate(null);
    //set up the stub
    when(userServiceMock.activateUser(anyInt())).thenReturn(expectedUser3);

    //checking http status, content type and content in JSON
    mockMvc.perform(post("/user/activate/{id}", 3))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
            .andExpect(jsonPath("$.id", is(3)))
            .andExpect(jsonPath("$.userType", is("USER")))
            .andExpect(jsonPath("$.userStatus", is("ACTIVE")))
            .andExpect(jsonPath("$.firstName", is("Taras")))
            .andExpect(jsonPath("$.secondName", is("Haidychuk")))
            .andExpect(jsonPath("$.createdDate", is("2017-08-10 06:38:51")))
            .andExpect(jsonPath("$.closedDate", is((String)null)));

    //verify number of method calling
    verify(userServiceMock, times(1)).activateUser(anyInt());
    //verify that other method of mocked object were not called
    verifyNoMoreInteractions(userServiceMock);
  }

  @Test
  public void testFindByUserType() throws Exception {
    //set up the stub
    when(userServiceMock.findByUserType(anyString()))
            .thenReturn(Arrays.asList(expectedUser1, expectedUser3));

    //checking http status, content type and content in JSON
    mockMvc.perform(get("/user/type/{userType}", "user"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
            //first user
            .andExpect(jsonPath("$[0].id", is(1)))
            .andExpect(jsonPath("$[0].userType", is("USER")))
            .andExpect(jsonPath("$[0].userStatus", is("ACTIVE")))
            .andExpect(jsonPath("$[0].firstName", is("Vira")))
            .andExpect(jsonPath("$[0].secondName", is("Kuchma")))
            .andExpect(jsonPath("$[0].login", is("vira")))
            .andExpect(jsonPath("$[0].createdDate", is("2017-08-24 14:28:34")))
            .andExpect(jsonPath("$[0].closedDate", is((String)null)))
            //second user
            .andExpect(jsonPath("$[1].id", is(3)))
            .andExpect(jsonPath("$[1].userType", is("USER")))
            .andExpect(jsonPath("$[1].userStatus", is("CLOSED")))
            .andExpect(jsonPath("$[1].firstName", is("Taras")))
            .andExpect(jsonPath("$[1].secondName", is("Haidychuk")))
            .andExpect(jsonPath("$[1].login", is("taras")))
            .andExpect(jsonPath("$[1].createdDate", is("2017-08-10 06:38:51")))
            .andExpect(jsonPath("$[1].closedDate", is("2017-09-05 13:08:21")));

    //verify number of method calling
    verify(userServiceMock, times(1)).findByUserType(anyString());
    //verify that other method of mocked object were not called
    verifyNoMoreInteractions(userServiceMock);
  }

  @Test
  public void testFindByUserStatus() throws Exception {
    //set up the stub
    when(userServiceMock.findByUserStatus(anyString()))
            .thenReturn(Arrays.asList(expectedUser1, expectedUser3));

    //checking http status, content type and content in JSON
    mockMvc.perform(get("/user/status/{userStatus}", "active"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
            //first user
            .andExpect(jsonPath("$[0].id", is(1)))
            .andExpect(jsonPath("$[0].userType", is("USER")))
            .andExpect(jsonPath("$[0].userStatus", is("ACTIVE")))
            .andExpect(jsonPath("$[0].firstName", is("Vira")))
            .andExpect(jsonPath("$[0].secondName", is("Kuchma")))
            .andExpect(jsonPath("$[0].login", is("vira")))
            .andExpect(jsonPath("$[0].createdDate", is("2017-08-24 14:28:34")))
            .andExpect(jsonPath("$[0].closedDate", is((String)null)))
            //second user
            .andExpect(jsonPath("$[1].id", is(3)))
            .andExpect(jsonPath("$[1].userType", is("USER")))
            .andExpect(jsonPath("$[1].userStatus", is("CLOSED")))
            .andExpect(jsonPath("$[1].firstName", is("Taras")))
            .andExpect(jsonPath("$[1].secondName", is("Haidychuk")))
            .andExpect(jsonPath("$[1].login", is("taras")))
            .andExpect(jsonPath("$[1].createdDate", is("2017-08-10 06:38:51")))
            .andExpect(jsonPath("$[1].closedDate", is("2017-09-05 13:08:21")));

    //verify number of method calling
    verify(userServiceMock, times(1)).findByUserStatus(anyString());
    //verify that other method of mocked object were not called
    verifyNoMoreInteractions(userServiceMock);
  }

  @Test
  public void testFindByFirstName() throws Exception {
    //set up the stub
    when(userServiceMock.findByFirstName(anyString()))
            .thenReturn(Arrays.asList(expectedUser1));

    //checking http status, content type and content in JSON
    mockMvc.perform(get("/user/firstName/{firstName}", "Vira"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
            //first user
            .andExpect(jsonPath("$[0].id", is(1)))
            .andExpect(jsonPath("$[0].userType", is("USER")))
            .andExpect(jsonPath("$[0].userStatus", is("ACTIVE")))
            .andExpect(jsonPath("$[0].firstName", is("Vira")))
            .andExpect(jsonPath("$[0].secondName", is("Kuchma")))
            .andExpect(jsonPath("$[0].login", is("vira")))
            .andExpect(jsonPath("$[0].createdDate", is("2017-08-24 14:28:34")))
            .andExpect(jsonPath("$[0].closedDate", is((String)null)));

    //verify number of method calling
    verify(userServiceMock, times(1)).findByFirstName(anyString());
    //verify that other method of mocked object were not called
    verifyNoMoreInteractions(userServiceMock);
  }

  @Test
  public void testFindBySecondName() throws Exception {
    //set up the stub
    when(userServiceMock.findBySecondName(anyString()))
            .thenReturn(Arrays.asList(expectedUser1));

    //checking http status, content type and content in JSON
    mockMvc.perform(get("/user/secondName/{secondName}", "Kuchma"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
            //first user
            .andExpect(jsonPath("$[0].id", is(1)))
            .andExpect(jsonPath("$[0].userType", is("USER")))
            .andExpect(jsonPath("$[0].userStatus", is("ACTIVE")))
            .andExpect(jsonPath("$[0].firstName", is("Vira")))
            .andExpect(jsonPath("$[0].secondName", is("Kuchma")))
            .andExpect(jsonPath("$[0].login", is("vira")))
            .andExpect(jsonPath("$[0].createdDate", is("2017-08-24 14:28:34")))
            .andExpect(jsonPath("$[0].closedDate", is((String)null)));

    //verify number of method calling
    verify(userServiceMock, times(1)).findBySecondName(anyString());
    //verify that other method of mocked object were not called
    verifyNoMoreInteractions(userServiceMock);
  }

  @Test
  public void testFindByCreatedDate() throws Exception {
    //set up the stub
    when(userServiceMock.findByCreatedDate(any(Date.class)))
            .thenReturn(Arrays.asList(expectedUser1));

    //checking http status, content type and content in JSON
    mockMvc.perform(get("/user/createdDate/{createdDate}", new Date("24 Aug 2017").getTime()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
            //first user
            .andExpect(jsonPath("$[0].id", is(1)))
            .andExpect(jsonPath("$[0].userType", is("USER")))
            .andExpect(jsonPath("$[0].userStatus", is("ACTIVE")))
            .andExpect(jsonPath("$[0].firstName", is("Vira")))
            .andExpect(jsonPath("$[0].secondName", is("Kuchma")))
            .andExpect(jsonPath("$[0].login", is("vira")))
            .andExpect(jsonPath("$[0].createdDate", is("2017-08-24 14:28:34")))
            .andExpect(jsonPath("$[0].closedDate", is((String)null)));

    //verify number of method calling
    verify(userServiceMock, times(1)).findByCreatedDate(any(Date.class));
    //verify that other method of mocked object were not called
    verifyNoMoreInteractions(userServiceMock);
  }

  @Test
  public void testFindByClosedDate() throws Exception {
    //set up the stub
    when(userServiceMock.findByClosedDate(any(Date.class)))
            .thenReturn(Arrays.asList(expectedUser3));

    //checking http status, content type and content in JSON
    mockMvc.perform(get("/user/closedDate/{closedDate}", new Date("05 Sep 2017").getTime()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
            //first user
            .andExpect(jsonPath("$[0].id", is(3)))
            .andExpect(jsonPath("$[0].userType", is("USER")))
            .andExpect(jsonPath("$[0].userStatus", is("CLOSED")))
            .andExpect(jsonPath("$[0].firstName", is("Taras")))
            .andExpect(jsonPath("$[0].secondName", is("Haidychuk")))
            .andExpect(jsonPath("$[0].login", is("taras")))
            .andExpect(jsonPath("$[0].createdDate", is("2017-08-10 06:38:51")))
            .andExpect(jsonPath("$[0].closedDate", is("2017-09-05 13:08:21")));

    //verify number of method calling
    verify(userServiceMock, times(1)).findByClosedDate(any(Date.class));
    //verify that other method of mocked object were not called
    verifyNoMoreInteractions(userServiceMock);
  }
}
