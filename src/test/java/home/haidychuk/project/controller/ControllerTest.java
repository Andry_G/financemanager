package home.haidychuk.project.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import home.haidychuk.project.GeneralTest;
import org.junit.Before;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

/**
 * General test class for all controllers.
 *
 * @author Andrii Hiadychuk created 10.05.2018
 * @version 1.0
 */
public abstract class ControllerTest extends GeneralTest {
  // *** Fields *** //

  @Autowired
  private WebApplicationContext wac;
  protected MockMvc mockMvc;

  @Before
  public void setup() {
    mockMvc = MockMvcBuilders.webAppContextSetup(wac).build();
  }

  protected <T> String convertObjectToJson(T obj) throws JsonProcessingException {
    ObjectMapper mapper = new ObjectMapper();

    return mapper.writeValueAsString(obj);
  }
}
