package home.haidychuk.project;

import static home.haidychuk.project.model.util.DateUtil.dateWithRequestedDay;

import home.haidychuk.project.model.entity.Account;
import home.haidychuk.project.model.entity.Budget;
import home.haidychuk.project.model.entity.Category;
import home.haidychuk.project.model.entity.CategoryGroup;
import home.haidychuk.project.model.entity.Currency;
import home.haidychuk.project.model.entity.Operation;
import home.haidychuk.project.model.entity.User;
import home.haidychuk.project.model.util.DateUtil;

import java.math.BigDecimal;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;

import org.junit.Before;
import org.junit.runner.RunWith;

import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.jdbc.SqlGroup;
import org.springframework.test.context.junit4.SpringRunner;

/**
 * General test class.
 * @author Andrii Hiadychuk created 04.05.2018
 * @version 1.0
 */
@RunWith(SpringRunner.class)
@SpringBootTest
@SqlGroup({
    @Sql(scripts = "/queries/init_data.sql",
          executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD
    ),
    @Sql(scripts = "/queries/clean_up_data_mysql.sql",
          executionPhase = Sql.ExecutionPhase.AFTER_TEST_METHOD
    )
})
@ActiveProfiles("mysql")
public abstract class GeneralTest {
  // *** Fields *** //

  // Currencies for testing
  protected Currency expectedCurrency1;
  protected Currency expectedCurrency2;
  protected Currency expectedCurrency3;

  // Users for testing
  protected User expectedUser1;
  protected User expectedUser2;
  protected User expectedUser3;

  // Accounts for testing
  protected Account expectedAccount1;
  protected Account expectedAccount2;
  protected Account expectedAccount3;
  protected Account expectedAccount4;

  // CategoryGroup for testing
  protected CategoryGroup expectedCategoryGroup1;
  protected CategoryGroup expectedCategoryGroup2;

  // Category for testing
  protected Category expectedCategory1;
  protected Category expectedCategory2;
  protected Category expectedCategory3;
  protected Category expectedCategory4;

  // Operation for testing
  protected Operation expectedOperation1;
  protected Operation expectedOperation2;

  // Budget for testing
  protected Budget expectedBudget1;

  /**
   * Initialisation all test Entities.
   */
  @Before
  public void init() {
    // initialisation currencies
    expectedCurrency1 = new Currency("UAH", "Гривня");
    expectedCurrency1.setId(1);

    expectedCurrency2 = new Currency("USD", "Долар США");
    expectedCurrency2.setId(2);

    expectedCurrency3 = new Currency("EUR", "Євро");
    expectedCurrency3.setId(3);

    // initialisation users
    expectedUser1 = new User(
            User.UserType.USER, User.UserStatus.ACTIVE,
            "Vira", "Kuchma",
            "vira", "virakuch",
            new Date("24 Aug 2017 17:28:34"), null
    );
    expectedUser1.setId(1);

    expectedUser2 = new User(
            User.UserType.ADMIN, User.UserStatus.ACTIVE,
            "Andrii", "Haidychuk",
            "andrii", "andrhaid",
            new Date("30 Jul 2017 13:16:42"), null
    );
    expectedUser2.setId(2);

    expectedUser3 = new User(
            User.UserType.USER, User.UserStatus.CLOSED,
            "Taras", "Haidychuk",
            "taras", "tarhaid",
            new Date("10 Aug 2017 09:38:51"),
            new Date("05 Sep 2017 16:08:21")
    );
    expectedUser3.setId(3);

    // initialisation accounts
    expectedAccount1 = new Account(
            expectedUser1, expectedCurrency1, "Готівка,грн.", new BigDecimal("1000.00"), true
    );
    expectedAccount1.setId(1);

    expectedAccount2 = new Account(
            expectedUser1, expectedCurrency2, "Готівка,дол.", new BigDecimal("500.00"), true
    );
    expectedAccount2.setId(2);

    expectedAccount3 = new Account(
            expectedUser1, expectedCurrency3, "Готівка,євро.", new BigDecimal("700.00"), true
    );
    expectedAccount3.setId(3);

    expectedAccount4 = new Account(
            expectedUser3, expectedCurrency1, "Депозит,грн.", new BigDecimal("15000.00"), true
    );
    expectedAccount4.setId(4);

    // initialisation category groups
    expectedCategoryGroup1 = new CategoryGroup(expectedUser1, "Car");
    expectedCategoryGroup1.setId(1);

    expectedCategoryGroup2 = new CategoryGroup(expectedUser1, "Meal");
    expectedCategoryGroup2.setId(2);

    // initialisation categories
    expectedCategory1 = new Category(
            expectedCategoryGroup1, "Fuel", Category.CategoryType.EXPENSE);
    expectedCategory1.setId(1);

    expectedCategory2 = new Category(
            expectedCategoryGroup1, "Parking", Category.CategoryType.EXPENSE);
    expectedCategory2.setId(2);

    expectedCategory3 = new Category(
            expectedCategoryGroup2, "Breakfast", Category.CategoryType.EXPENSE);
    expectedCategory3.setId(3);

    expectedCategory4 = new Category(
            expectedCategoryGroup2, "Cafe", Category.CategoryType.EXPENSE);
    expectedCategory4.setId(4);

    // initialisation operations
    expectedOperation1 = new Operation(expectedCategory1, expectedAccount1, null,
            Operation.OperationType.EXPENSE, new Date("25 Nov 2017"),
            new BigDecimal("150.00"), new BigDecimal("1.00"));
    expectedOperation1.setId(1);

    expectedOperation2 = new Operation(expectedCategory2, null, expectedAccount1,
            Operation.OperationType.INCOME, new Date("01 Dec 2017"),
            new BigDecimal("10.00"), new BigDecimal("1.00"));
    expectedOperation2.setId(2);
    expectedOperation2.setComment("test income operation");

    // initialization budgets
    expectedBudget1 = new Budget(expectedUser1, expectedCurrency1, "CarBudget",
            new BigDecimal("500.00"), Budget.BudgetType.EXPENSE, Budget.Range.CURRENT_MONTH);
    expectedBudget1.setId(1);
    expectedBudget1.setByDateRange(false);
    expectedBudget1.setStartDate(dateWithRequestedDay(DateUtil.Day.FIRST_DAY_OF_MONTH));
    expectedBudget1.setEndDate(dateWithRequestedDay(DateUtil.Day.LAST_DAY_OF_MONTH));
    expectedBudget1.setAccounts(new ArrayList<>());
    expectedBudget1.getAccounts().add(expectedAccount1);
    expectedBudget1.setCategoryGroups(new ArrayList<>());
    expectedBudget1.getCategoryGroups().add(expectedCategoryGroup1);
    expectedBudget1.setCategories(new ArrayList<>());
    expectedBudget1.getCategories().addAll(Arrays.asList(expectedCategory1, expectedCategory2));
  }
}
