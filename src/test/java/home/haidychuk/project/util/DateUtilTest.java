package home.haidychuk.project.util;

import static home.haidychuk.project.model.util.DateUtil.dateWithRequestedDay;

import static org.junit.Assert.assertEquals;

import home.haidychuk.project.model.util.DateUtil;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;

import java.util.Calendar;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;


/**
 * Test class for DateUtil.
 * @author Andrii Haidychuk created on 02.03.2018
 * @version 1.0
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class DateUtilTest {
  // *** Fields *** //

  private static Calendar calendar = Calendar.getInstance();

  @Test
  public void testDateWithRequestedDay() throws ParseException {
    Calendar cal = Calendar.getInstance();
    DateFormat df = new SimpleDateFormat("yyyy-MM-dd");

    //first and last days of current week
    cal.setTimeInMillis(calendar.getTimeInMillis());
    cal.set(Calendar.DAY_OF_WEEK, 2);
    assertEquals(df.parse(df.format(cal.getTime())),
            dateWithRequestedDay(DateUtil.Day.FIRST_DAY_OF_WEEK));

    cal.set(Calendar.DAY_OF_WEEK, 1);
    assertEquals(df.parse(df.format(cal.getTime())),
            dateWithRequestedDay(DateUtil.Day.LAST_DAY_OF_WEEK));

    //first and last days of last week
    cal.setTimeInMillis(calendar.getTimeInMillis());
    cal.add(Calendar.WEEK_OF_MONTH, -1);
    cal.set(Calendar.DAY_OF_WEEK, 2);
    assertEquals(df.parse(df.format(cal.getTime())),
            dateWithRequestedDay(DateUtil.Day.FIRST_DAY_OF_LAST_WEEK));

    cal.set(Calendar.DAY_OF_WEEK, 1);
    assertEquals(df.parse(df.format(cal.getTime())),
            dateWithRequestedDay(DateUtil.Day.LAST_DAY_OF_LAST_WEEK));

    //first and last days of current month
    cal.setTimeInMillis(calendar.getTimeInMillis());
    cal.set(Calendar.DAY_OF_MONTH, 1);
    assertEquals(df.parse(df.format(cal.getTime())),
            dateWithRequestedDay(DateUtil.Day.FIRST_DAY_OF_MONTH));

    cal.set(Calendar.DAY_OF_MONTH, cal.getActualMaximum(Calendar.DAY_OF_MONTH));
    assertEquals(df.parse(df.format(cal.getTime())),
            dateWithRequestedDay(DateUtil.Day.LAST_DAY_OF_MONTH));

    //first and last days of last month
    cal.setTimeInMillis(calendar.getTimeInMillis());
    cal.add(Calendar.MONTH, -1);
    cal.set(Calendar.DAY_OF_MONTH, 1);
    assertEquals(df.parse(df.format(cal.getTime())),
            dateWithRequestedDay(DateUtil.Day.FIRST_DAY_OF_LAST_MONTH));

    cal.set(Calendar.DAY_OF_MONTH, cal.getActualMaximum(Calendar.DAY_OF_MONTH));
    assertEquals(df.parse(df.format(cal.getTime())),
            dateWithRequestedDay(DateUtil.Day.LAST_DAY_OF_LAST_MONTH));

    //first and last days of current year
    cal.setTimeInMillis(calendar.getTimeInMillis());
    cal.set(Calendar.DAY_OF_YEAR, 1);
    assertEquals(df.parse(df.format(cal.getTime())),
            dateWithRequestedDay(DateUtil.Day.FIRST_DAY_OF_YEAR));

    cal.set(Calendar.DAY_OF_YEAR, cal.getActualMaximum(Calendar.DAY_OF_YEAR));
    assertEquals(df.parse(df.format(cal.getTime())),
            dateWithRequestedDay(DateUtil.Day.LAST_DAY_OF_YEAR));

    //first and last days of last year
    cal.setTimeInMillis(calendar.getTimeInMillis());
    cal.add(Calendar.YEAR, -1);
    cal.set(Calendar.DAY_OF_YEAR, 1);
    assertEquals(df.parse(df.format(cal.getTime())),
            dateWithRequestedDay(DateUtil.Day.FIRST_DAY_OF_LAST_YEAR));

    cal.set(Calendar.DAY_OF_YEAR, cal.getActualMaximum(Calendar.DAY_OF_YEAR));
    assertEquals(df.parse(df.format(cal.getTime())),
            dateWithRequestedDay(DateUtil.Day.LAST_DAY_OF_LAST_YEAR));
  }
}
