package home.haidychuk.project.decryption;

import static org.junit.Assert.assertEquals;

import org.junit.Test;
import org.junit.runner.RunWith;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.core.env.Environment;
import org.springframework.test.context.junit4.SpringRunner;


/**
 * Test class for decryption.
 * @author Andrii Haidychuk created on 21.03.2017
 * @version 1.0
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class DecryptionTest {

  @Autowired
  private Environment environment;

  @Test
  public void testDecryptionOfProperties() {
    assertEquals("finManagerUser", environment.getProperty("spring.datasource.username"));
    assertEquals("finmanager_access", environment.getProperty("spring.datasource.password"));
  }
}
