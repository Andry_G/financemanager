package home.haidychuk.project.repository;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import home.haidychuk.project.model.entity.Category;
import home.haidychuk.project.model.entity.Operation;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;

import org.junit.After;
import org.junit.Test;

import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.transaction.annotation.Transactional;


/**
 * Test class for Category class.
 * @author Andrii Haidychuk created on 26.11.2017
 * @version 1.0
 */
public class CategoryRepositoryTest extends RepositoryTest {
  // *** Fields *** //

  private Category categoryFromDb;

  /**
   * Clearing data.
   */
  @After
  public void deInit() {
    categoryFromDb = null;
  }

  @Test
  public void testFindOne() {
    categoryFromDb = categoryRepository.findOne(1);

    assertEquals(expectedCategory1, categoryFromDb);
  }

  @Test
  public void testFindAll() {
    List<Category> categoriesExpected = new ArrayList<>(Arrays.asList(
            expectedCategory1, expectedCategory2, expectedCategory3,expectedCategory4));

    List<Category> categoriesFromDb = new ArrayList<>(categoryRepository.findAll());


    assertEquals(categoriesExpected, categoriesFromDb);
  }

  @Test
  public void testSave() {
    Category expectedCategoryNew =
            new Category(expectedCategoryGroup1, "Washing", Category.CategoryType.EXPENSE);
    expectedCategoryNew.setId(5);
    categoryRepository.save(expectedCategoryNew);

    categoryFromDb = categoryRepository.findOne(5);

    assertEquals(expectedCategoryNew, categoryFromDb);
  }

  @Test
  @Transactional
  public void testDelete() {
    categoryFromDb = categoryRepository.findOne(1);

    // delete all Operations for deleting Category
    Collection<Operation> operations = categoryFromDb.getOperations();
    operations.forEach((op) -> operationRepository.delete(op.getId()));

    categoryRepository.delete(1);

    assertNull(categoryRepository.findOne(1));
  }

  /**
   * Test operation of deleting if children entries exist.
   */
  @Test(expected = DataIntegrityViolationException.class)
  public void testDeleteConstraint() {
    categoryRepository.delete(1);

    assertNull(categoryRepository.findOne(1));
  }

  @Test
  public void testDeleteAll() {
    operationRepository.deleteAll();
    categoryRepository.deleteAll();

    assertTrue(categoryRepository.findAll().isEmpty());
  }

  /**
   * Test operation of deleting for all categories if children entries exist.
   */
  @Test(expected = DataIntegrityViolationException.class)
  public void testDeleteAllConstraint() {
    categoryRepository.deleteAll();

    assertNull(categoryRepository.findAll());
  }

  @Test
  public void testFindByName() {
    categoryFromDb = categoryRepository.findByName("Cafe");

    assertEquals(expectedCategory4, categoryFromDb);
  }
}
