package home.haidychuk.project.repository;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import home.haidychuk.project.model.entity.Operation;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import org.junit.After;
import org.junit.Test;



/**
 * Test class for Operation class.
 * @author Andrii Haidychuk 29.11.2017
 * @version 1.0
 */
public class OperationRepositoryTest extends RepositoryTest {
  // *** Fields *** //

  private Operation operationFromDb;

  @After
  public void deInit() {
    operationFromDb = null;
  }

  @Test
  public void testFindOne() {
    operationFromDb = operationRepository.findOne(1);

    assertEquals(expectedOperation1, operationFromDb);
  }

  @Test
  public void testFindAll() {
    List<Operation> operationsExpected = new ArrayList<>(Arrays.asList(
            expectedOperation1, expectedOperation2));

    List<Operation> operationsFromDb = new ArrayList<>(operationRepository.findAll());

    assertEquals(operationsExpected, operationsFromDb);
  }

  @Test
  public void testSave() {
    Operation expectedOperationNew = new Operation(expectedCategory2, expectedAccount1, null,
            Operation.OperationType.EXPENSE, new Date("10 Oct 2017"),
            new BigDecimal("5.02"), new BigDecimal("1.00"));
    expectedOperationNew.setId(3);

    operationRepository.save(expectedOperationNew);

    operationFromDb = operationRepository.findOne(3);

    assertEquals(expectedOperationNew, operationFromDb);
  }

  @Test
  public void testDelete() {
    operationRepository.delete(1);

    assertNull(operationRepository.findOne(1));
  }

  @Test
  public void testDeleteAll() {
    operationRepository.deleteAll();

    assertTrue(operationRepository.findAll().isEmpty());
  }

  @Test
  public void testFindByOperationTypeAndSource() {
    List<Operation> operationsExpected = new ArrayList<>(Arrays.asList(expectedOperation2));
    Collections.sort(operationsExpected);

    List<Operation> operationsFromDb = new ArrayList<>(
            operationRepository.findByOperationType(Operation.OperationType.INCOME));
    Collections.sort(operationsFromDb);

    assertEquals(operationsExpected, operationsFromDb);
  }
}
