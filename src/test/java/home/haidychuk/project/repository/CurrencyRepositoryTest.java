package home.haidychuk.project.repository;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import home.haidychuk.project.model.entity.Account;
import home.haidychuk.project.model.entity.Currency;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;

import org.junit.After;
import org.junit.Test;

import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.transaction.annotation.Transactional;

/**
 * Test class for CurrencyRepository class.
 * @author Andrii Haidychuk created on 28.08.2017
 * @version 1.0
 */
public class CurrencyRepositoryTest extends RepositoryTest {
  // *** Fields *** //
  private Currency currencyFromDb;

  /**
   * Clearing data.
   */
  @After
  public void deInit() {
    currencyFromDb = null;
  }

  @Test
  public void testFindOne() {
    currencyFromDb = currencyRepository.findOne(1);

    assertEquals(expectedCurrency1, currencyFromDb);
  }

  @Test
  public void testFindAll() {
    List<Currency> listExpected = new ArrayList<>(
            Arrays.asList(expectedCurrency1, expectedCurrency2, expectedCurrency3));
    List<Currency> listFromDb = currencyRepository.findAll();

    assertEquals(listExpected, listFromDb);
  }

  @Test
  public void testSave() {
    Currency currencyExpected = new Currency();
    currencyExpected.setId(4);
    currencyExpected.setShortTitle("JPY");
    currencyExpected.setTitle("Японська Єна");

    currencyRepository.save(currencyExpected);
    currencyFromDb = currencyRepository.findOne(4);

    assertEquals(currencyExpected, currencyFromDb);
  }

  @Test
  @Transactional
  public void testDelete() {
    currencyFromDb = currencyRepository.findOne(2);

    // delete all Accounts and their Operations for deleting Currency
    Collection<Account> accounts = currencyFromDb.getAccounts();
    accounts.forEach((acc) -> {
      acc.getSourceOperations().forEach(s -> operationRepository.delete(s.getId()));
      acc.getDestinationOperations().forEach(dest -> operationRepository.delete(dest.getId()));
      accountRepository.delete(acc.getId());
    });

    currencyRepository.delete(2);

    assertNull(currencyRepository.findOne(2));
  }

  /**
   * Test operation of deleting if children entries exist.
   */
  @Test(expected = DataIntegrityViolationException.class)
  public void testDeleteConstraint() {
    currencyRepository.delete(1);

    assertNull(currencyRepository.findOne(1));
  }

  @Test
  public void testDeleteAll() {
    operationRepository.deleteAll();
    accountRepository.deleteAll();
    budgetRepository.deleteAll();
    currencyRepository.deleteAll();

    assertTrue(currencyRepository.findAll().isEmpty());
  }

  /**
   * Test operation of deleting for all currencies if children entries exist.
   */
  @Test(expected = DataIntegrityViolationException.class)
  public void testDeleteAllConstraint() {
    currencyRepository.deleteAll();

    assertNull(currencyRepository.findAll());
  }

  @Test
  public void testFindByShortTitle() {
    assertEquals(expectedCurrency3, currencyRepository.findByShortTitle("EUR"));
  }

  @Test
  public void testFindByTitle() {
    assertEquals(expectedCurrency3, currencyRepository.findByTitle("Євро"));
  }
}
