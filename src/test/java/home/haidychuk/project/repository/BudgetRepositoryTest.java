package home.haidychuk.project.repository;

import static home.haidychuk.project.model.util.DateUtil.dateWithRequestedDay;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import home.haidychuk.project.model.entity.Account;
import home.haidychuk.project.model.entity.Budget;
import home.haidychuk.project.model.entity.Category;
import home.haidychuk.project.model.entity.CategoryGroup;
import home.haidychuk.project.model.util.DateUtil;

import java.math.BigDecimal;

import java.text.ParseException;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import org.junit.After;
import org.junit.Test;

import org.springframework.transaction.annotation.Transactional;

/**
 * Test class for BudgetRepository class.
 * @author Andrii Haidychuk created on 07.12.2017
 * @version 1.0
 */
public class BudgetRepositoryTest extends RepositoryTest {
  // *** Fields *** //
  private Budget budgetFromDb;

  /**
   * Clearing data.
   */
  @After
  public void deInit() {
    budgetFromDb = null;
  }

  @Test
  public void testFindOne() {
    budgetFromDb = budgetRepository.findOne(1);

    assertEquals(expectedBudget1, budgetFromDb);
  }

  @Test
  public void testFindAll() {
    List<Budget> budgetsExpected = new ArrayList<>(Arrays.asList(expectedBudget1));

    List<Budget> budgetsFromDb = budgetRepository.findAll();

    assertEquals(budgetsExpected, budgetsFromDb);
  }

  @Test
  public void testSave() {
    Budget newBudget = new Budget(expectedUser1, expectedCurrency2, "NewBudget",
            new BigDecimal("300.00"), Budget.BudgetType.PROFITABLE, Budget.Range.CURRENT_MONTH);
    newBudget.setId(2);
    newBudget.setByDateRange(false);
    newBudget.setStartDate(dateWithRequestedDay(DateUtil.Day.FIRST_DAY_OF_MONTH));
    newBudget.setEndDate(dateWithRequestedDay(DateUtil.Day.LAST_DAY_OF_MONTH));
    budgetRepository.save(newBudget);

    budgetFromDb = budgetRepository.findOne(2);

    assertEquals(newBudget, budgetFromDb);
  }

  @Test
  public void testDelete() {
    budgetRepository.delete(1);

    assertNull(budgetRepository.findOne(1));
  }

  @Test
  public void testDeleteAll() {
    budgetRepository.deleteAll();

    assertTrue(budgetRepository.findAll().isEmpty());
  }

  /**
   * Test for prePersistValues method in Budget entity.
   */
  @Test
  public void testPrePersistValues() throws ParseException {
    Budget createdBudget = new Budget(expectedUser1, expectedCurrency1, "Meals",
            new BigDecimal("450.00"));
    createdBudget.setId(2);

    budgetRepository.save(createdBudget);
    budgetFromDb = budgetRepository.findOne(2);

    assertTrue(budgetFromDb.getStartDate()
                       .equals(dateWithRequestedDay(DateUtil.Day.FIRST_DAY_OF_MONTH)));
    assertTrue(budgetFromDb.getEndDate()
                       .equals(dateWithRequestedDay(DateUtil.Day.LAST_DAY_OF_MONTH)));
    assertTrue(budgetFromDb.getBudgetType() == Budget.BudgetType.EXPENSE);
    assertTrue(budgetFromDb.getTimeRange() == Budget.Range.CURRENT_MONTH);
    assertTrue(!budgetFromDb.getByDateRange());
  }

  /**
   * Test for preUpdateValues method in Budget entity.
   */
  @Test
  public void testPreUpdateValues() {
    Budget existBudget = budgetRepository.findOne(1);

    /* testing for value byDateRange = true */
    existBudget.setByDateRange(true);
    existBudget.setStartDate(new Date("05 Mar 2018"));
    existBudget.setEndDate(new Date("22 Mar 2018"));
    //update Budget
    budgetRepository.save(existBudget);
    budgetFromDb = budgetRepository.findOne(1);

    assertTrue(budgetFromDb.getByDateRange());
    assertTrue(budgetFromDb.getStartDate().equals(new Date("05 Mar 2018")));
    assertTrue(budgetFromDb.getEndDate().equals(new Date("22 Mar 2018")));
    assertNull(budgetFromDb.getTimeRange());

    /* testing for value CURRENT_WEEK */
    existBudget = budgetRepository.findOne(1);
    existBudget.setByDateRange(false);
    existBudget.setBudgetType(Budget.BudgetType.PROFITABLE);
    existBudget.setTimeRange(Budget.Range.CURRENT_WEEK);
    //update Budget
    budgetRepository.save(existBudget);
    budgetFromDb = budgetRepository.findOne(1);

    assertTrue(budgetFromDb.getStartDate()
                       .equals(dateWithRequestedDay(DateUtil.Day.FIRST_DAY_OF_WEEK)));
    assertTrue(budgetFromDb.getEndDate()
                       .equals(dateWithRequestedDay(DateUtil.Day.LAST_DAY_OF_WEEK)));
    assertTrue(budgetFromDb.getBudgetType() == Budget.BudgetType.PROFITABLE);
    assertTrue(budgetFromDb.getTimeRange() == Budget.Range.CURRENT_WEEK);
    assertTrue(!budgetFromDb.getByDateRange());

    /* testing for value LAST_WEEK */
    existBudget = budgetRepository.findOne(1);
    existBudget.setBudgetType(Budget.BudgetType.PROFITABLE);
    existBudget.setTimeRange(Budget.Range.LAST_WEEK);
    //update Budget
    budgetRepository.save(existBudget);
    budgetFromDb = budgetRepository.findOne(1);

    assertTrue(budgetFromDb.getStartDate()
                       .equals(dateWithRequestedDay(DateUtil.Day.FIRST_DAY_OF_LAST_WEEK)));
    assertTrue(budgetFromDb.getEndDate()
                       .equals(dateWithRequestedDay(DateUtil.Day.LAST_DAY_OF_LAST_WEEK)));
    assertTrue(budgetFromDb.getBudgetType() == Budget.BudgetType.PROFITABLE);
    assertTrue(budgetFromDb.getTimeRange() == Budget.Range.LAST_WEEK);
    assertTrue(!budgetFromDb.getByDateRange());

    /* testing for value CURRENT_MONTH */
    existBudget = budgetRepository.findOne(1);
    existBudget.setBudgetType(Budget.BudgetType.UNIVERSAL);
    existBudget.setTimeRange(Budget.Range.CURRENT_MONTH);
    //update Budget
    budgetRepository.save(existBudget);
    budgetFromDb = budgetRepository.findOne(1);

    assertTrue(budgetFromDb.getStartDate()
                       .equals(dateWithRequestedDay(DateUtil.Day.FIRST_DAY_OF_MONTH)));
    assertTrue(budgetFromDb.getEndDate()
                       .equals(dateWithRequestedDay(DateUtil.Day.LAST_DAY_OF_MONTH)));
    assertTrue(budgetFromDb.getBudgetType() == Budget.BudgetType.UNIVERSAL);
    assertTrue(budgetFromDb.getTimeRange() == Budget.Range.CURRENT_MONTH);
    assertTrue(!budgetFromDb.getByDateRange());

    /* testing for value LAST_MONTH */
    existBudget = budgetRepository.findOne(1);
    existBudget.setBudgetType(Budget.BudgetType.UNIVERSAL);
    existBudget.setTimeRange(Budget.Range.LAST_MONTH);
    //update Budget
    budgetRepository.save(existBudget);
    budgetFromDb = budgetRepository.findOne(1);

    assertTrue(budgetFromDb.getStartDate()
                       .equals(dateWithRequestedDay(DateUtil.Day.FIRST_DAY_OF_LAST_MONTH)));
    assertTrue(budgetFromDb.getEndDate()
                       .equals(dateWithRequestedDay(DateUtil.Day.LAST_DAY_OF_LAST_MONTH)));
    assertTrue(budgetFromDb.getBudgetType() == Budget.BudgetType.UNIVERSAL);
    assertTrue(budgetFromDb.getTimeRange() == Budget.Range.LAST_MONTH);
    assertTrue(!budgetFromDb.getByDateRange());

    /* testing for value CURRENT_YEAR */
    existBudget = budgetRepository.findOne(1);
    existBudget.setBudgetType(Budget.BudgetType.EXPENSE);
    existBudget.setTimeRange(Budget.Range.CURRENT_YEAR);
    //update Budget
    budgetRepository.save(existBudget);
    budgetFromDb = budgetRepository.findOne(1);

    assertTrue(budgetFromDb.getStartDate()
                       .equals(dateWithRequestedDay(DateUtil.Day.FIRST_DAY_OF_YEAR)));
    assertTrue(budgetFromDb.getEndDate()
                       .equals(dateWithRequestedDay(DateUtil.Day.LAST_DAY_OF_YEAR)));
    assertTrue(budgetFromDb.getBudgetType() == Budget.BudgetType.EXPENSE);
    assertTrue(budgetFromDb.getTimeRange() == Budget.Range.CURRENT_YEAR);
    assertTrue(!budgetFromDb.getByDateRange());

    /* testing for value LAST_YEAR */
    existBudget = budgetRepository.findOne(1);
    existBudget.setBudgetType(Budget.BudgetType.PROFITABLE);
    existBudget.setTimeRange(Budget.Range.LAST_YEAR);
    //update Budget
    budgetRepository.save(existBudget);
    budgetFromDb = budgetRepository.findOne(1);

    assertTrue(budgetFromDb.getStartDate()
                       .equals(dateWithRequestedDay(DateUtil.Day.FIRST_DAY_OF_LAST_YEAR)));
    assertTrue(budgetFromDb.getEndDate()
                       .equals(dateWithRequestedDay(DateUtil.Day.LAST_DAY_OF_LAST_YEAR)));
    assertTrue(budgetFromDb.getBudgetType() == Budget.BudgetType.PROFITABLE);
    assertTrue(budgetFromDb.getTimeRange() == Budget.Range.LAST_YEAR);
    assertTrue(!budgetFromDb.getByDateRange());
  }

  @Test
  @Transactional
  public void testAddAccountToBudget() {
    //init expected Budget
    expectedBudget1.getAccounts().add(expectedAccount2);

    //init actual Budget
    Budget budget = budgetRepository.findOne(1);
    Account addAccount = accountRepository.findOne(2);
    budgetRepository.addAccountToBudget(budget, addAccount);

    //getting updated actual Budget
    budgetFromDb = budgetRepository.findOne(1);

    assertEquals(expectedBudget1, budgetFromDb);
    assertEquals(expectedBudget1.getAccounts(), budgetFromDb.getAccounts());
  }

  @Test
  @Transactional
  public void testRemoveAccountFromBudget() {
    //init expected Budget
    expectedBudget1.getAccounts().add(expectedAccount2);
    expectedBudget1.getAccounts().remove(expectedAccount1);

    //init actual Budget
    Budget budget = budgetRepository.findOne(1);
    Account addAccount = accountRepository.findOne(2);
    budgetRepository.addAccountToBudget(budget, addAccount);

    Account removedAccount = accountRepository.findOne(1);
    budgetRepository.removeAccountFromBudget(budget, removedAccount);

    //getting updated actual Budget
    budgetFromDb = budgetRepository.findOne(1);

    assertEquals(expectedBudget1, budgetFromDb);
    assertEquals(expectedBudget1.getAccounts(), budgetFromDb.getAccounts());
  }

  @Test
  @Transactional
  public void testAddCategoryGroupToBudget() {
    //init expected Budget
    expectedBudget1.getCategoryGroups().add(expectedCategoryGroup2);

    //init actual Budget
    Budget budget = budgetRepository.findOne(1);
    CategoryGroup addCategoryGroup = categoryGroupRepository.findOne(2);
    budgetRepository.addCategoryGroupToBudget(budget, addCategoryGroup);

    //getting updated actual Budget
    budgetFromDb = budgetRepository.findOne(1);

    assertEquals(expectedBudget1, budgetFromDb);
    assertEquals(expectedBudget1.getCategoryGroups(), budgetFromDb.getCategoryGroups());
  }

  @Test
  @Transactional
  public void testRemoveCategoryGroupFromBudget() {
    //init expected Budget
    expectedBudget1.getCategoryGroups().add(expectedCategoryGroup2);
    expectedBudget1.getCategoryGroups().remove(expectedCategoryGroup1);

    //init actual Budget
    Budget budget = budgetRepository.findOne(1);
    CategoryGroup addCategoryGroup = categoryGroupRepository.findOne(2);
    budgetRepository.addCategoryGroupToBudget(budget, addCategoryGroup);

    CategoryGroup removeCategoryGroup = categoryGroupRepository.findOne(1);
    budgetRepository.removeCategoryGroupFromBudget(budget, removeCategoryGroup);

    //getting updated actual Budget
    budgetFromDb = budgetRepository.findOne(1);

    assertEquals(expectedBudget1, budgetFromDb);
    assertEquals(expectedBudget1.getCategoryGroups(), budgetFromDb.getCategoryGroups());
  }

  @Test
  @Transactional
  public void testAddCategoryToBudget() {
    //init expected Budget
    expectedBudget1.getCategories().add(expectedCategory3);

    //init actual Budget
    Budget budget = budgetRepository.findOne(1);
    Category addCategory = categoryRepository.findOne(3);
    budgetRepository.addCategoryToBudget(budget, addCategory);

    //getting updated actual Budget
    budgetFromDb = budgetRepository.findOne(1);

    assertEquals(expectedBudget1, budgetFromDb);
    assertEquals(expectedBudget1.getCategories(), budgetFromDb.getCategories());
  }

  @Test
  @Transactional
  public void testRemoveCategoryFromBudget() {
    //init expected Budget
    expectedBudget1.getCategories().add(expectedCategory3);
    expectedBudget1.getCategories().remove(expectedCategory1);

    //init actual Budget
    Budget budget = budgetRepository.findOne(1);
    Category addCategory = categoryRepository.findOne(3);
    budgetRepository.addCategoryToBudget(budget, addCategory);

    Category removeCategory = categoryRepository.findOne(1);
    budgetRepository.removeCategoryFromBudget(budget, removeCategory);

    //getting updated actual Budget
    budgetFromDb = budgetRepository.findOne(1);

    assertEquals(expectedBudget1, budgetFromDb);
    assertEquals(expectedBudget1.getCategories(), budgetFromDb.getCategories());
  }
}
