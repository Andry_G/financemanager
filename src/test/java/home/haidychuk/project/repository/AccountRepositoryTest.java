package home.haidychuk.project.repository;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import home.haidychuk.project.model.entity.Account;
import home.haidychuk.project.model.entity.Operation;

import java.math.BigDecimal;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;

import org.junit.After;
import org.junit.Test;

import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.transaction.annotation.Transactional;

/**
 * Test class for CurrencyRepository class.
 * @author Andrii Haidychuk created on 17.09.2017
 * @version 1.0
 */
public class AccountRepositoryTest extends RepositoryTest {
  // *** Fields *** //
  private Account accountFromDb;

  /**
   * Clearing data.
   */
  @After
  public void deInit() {
    accountFromDb = null;
  }

  @Test
  public void testFindOne() {
    accountFromDb = accountRepository.findOne(1);

    assertEquals(expectedAccount1, accountFromDb);
  }

  @Test
  public void testFindAll() {
    List<Account> listExpected = new ArrayList<>(
            Arrays.asList(expectedAccount1, expectedAccount2, expectedAccount3, expectedAccount4));
    List<Account> listFromDb = accountRepository.findAll();

    assertEquals(listExpected, listFromDb);
  }

  @Test
  public void testSave() {
    Account accountExpected = new Account(
            expectedUser1, expectedCurrency2,
            "Депозит,дол.", new BigDecimal("5000.00"), true
    );
    accountExpected.setId(5);

    accountRepository.save(accountExpected);
    accountFromDb = accountRepository.findOne(5);

    assertEquals(accountExpected, accountFromDb);
  }

  @Test
  @Transactional
  public void testDelete() {
    accountFromDb = accountRepository.findOne(1);

    // delete all Operations where Account is source
    Collection<Operation> operations = accountFromDb.getSourceOperations();
    operations.forEach((op) -> operationRepository.delete(op.getId()));

    // delete all Operations where Account is destination
    operations = accountFromDb.getDestinationOperations();
    operations.forEach((op) -> operationRepository.delete(op.getId()));

    accountRepository.delete(4);

    assertNull(accountRepository.findOne(4));
  }

  /**
   * Test operation of deleting if children entries exist.
   */
  @Test(expected = DataIntegrityViolationException.class)
  public void testDeleteConstraint() {
    accountRepository.delete(1);

    assertNull(accountRepository.findOne(1));
  }

  @Test
  public void testDeleteAll() {
    operationRepository.deleteAll();
    accountRepository.deleteAll();

    assertTrue(accountRepository.findAll().isEmpty());
  }

  /**
   * Test operation of deleting for all accounts if children entries exist.
   */
  @Test(expected = DataIntegrityViolationException.class)
  public void testDeleteAllConstraint() {
    accountRepository.deleteAll();

    assertNull(accountRepository.findAll());
  }
}
