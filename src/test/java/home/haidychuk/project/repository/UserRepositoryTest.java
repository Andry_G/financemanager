package home.haidychuk.project.repository;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import home.haidychuk.project.model.entity.Account;
import home.haidychuk.project.model.entity.CategoryGroup;
import home.haidychuk.project.model.entity.User;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import org.junit.After;
import org.junit.Test;

import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.transaction.annotation.Transactional;


/**
 * Test class for UserRepository class.
 * @author Andrii Haidychuk created on 18.09.2017
 * @version 1.0
 */
public class UserRepositoryTest extends RepositoryTest {
  // *** Fields *** //
  private User userFromDb;

  /**
   * Clearing data.
   */
  @After
  public void deInit() {
    userFromDb = null;
  }

  @Test
  public void testFindOne() {
    userFromDb = userRepository.findOne(2);

    assertEquals(expectedUser2, userFromDb);
  }

  @Test
  public void testFindAll() {
    List<User> listExpected =
            new ArrayList<>(Arrays.asList(expectedUser1, expectedUser2, expectedUser3));
    List<User> listFromDb = userRepository.findAll();

    assertEquals(listExpected, listFromDb);
  }

  @Test
  public void testSave() {
    User userExpected = new User(
            User.UserType.USER, User.UserStatus.ACTIVE,
            "Olga", "Gumenyuk",
            "olga", "olggumen",
            new Date("25 Sep 2017 13:43:02")

    );
    userExpected.setId(4);

    userRepository.save(userExpected);
    userFromDb = userRepository.findOne(4);

    assertEquals(userExpected, userFromDb);
  }

  /**
   * Test operation of deleting if children entries doesn't exist.
   */
  @Test
  @Transactional
  public void testDelete() {
    userFromDb = userRepository.findOne(1);

    /* delete all Accounts and their Operation for deleting User
     * where this Account is source or destination
     */
    Collection<Account> accounts = userFromDb.getAccounts();
    accounts.forEach((acc) -> {
      // account is source
      acc.getSourceOperations().forEach((sour) -> operationRepository.delete(sour.getId()));
      // account is destination
      acc.getDestinationOperations().forEach((dest) -> operationRepository.delete(dest.getId()));
      accountRepository.delete(acc.getId());
    });

    // delete all CategoryGroup, Category for deleting User
    Collection<CategoryGroup> categoryGroups = userFromDb.getCategoryGroups();
    categoryGroups.forEach((cg) -> {
      cg.getCategories().forEach((cat) -> categoryRepository.delete(cat.getId()));
      categoryGroupRepository.delete(cg.getId());
    });

    userRepository.delete(1);

    assertNull(userRepository.findOne(1));
  }

  /**
   * Test operation of deleting if children entries exist.
   */
  @Test(expected = DataIntegrityViolationException.class)
  public void testDeleteConstraint() {
    userRepository.delete(1);

    assertNull(userRepository.findOne(1));
  }

  /**
   * Test operation of deleting for all Users if children entries doesn't exist.
   */
  @Test
  public void testDeleteAll() {
    operationRepository.deleteAll();
    accountRepository.deleteAll();
    categoryRepository.deleteAll();
    categoryGroupRepository.deleteAll();
    budgetRepository.deleteAll();

    userRepository.deleteAll();

    assertTrue(userRepository.findAll().isEmpty());
  }

  /**
   * Test operation of deleting for all Users if children entries exist.
   */
  @Test(expected = DataIntegrityViolationException.class)
  public void testDeleteAllConstraint() {
    userRepository.deleteAll();

    assertTrue(userRepository.findAll().isEmpty());
  }

  @Test
  public void testFindByUserType() {
    List<User> usersFromDb =
            new ArrayList<>(userRepository.findByUserType(User.UserType.ADMIN));
    Collections.sort(usersFromDb);

    List<User> usersExpected = new ArrayList<>(Arrays.asList(expectedUser2));
    Collections.sort(usersExpected);

    assertEquals(usersExpected, usersFromDb);
  }

  @Test
  public void testFindByUserStatus() {
    List<User> usersFromDb =
            new ArrayList<>(userRepository.findByUserStatus(User.UserStatus.ACTIVE));
    Collections.sort(usersFromDb);

    List<User> usersExpected = new ArrayList<>(Arrays.asList(expectedUser1, expectedUser2));
    Collections.sort(usersExpected);

    assertEquals(usersExpected, usersFromDb);
  }

  @Test
  public void testFindByFirstName() {
    List<User> usersFromDb =
            new ArrayList<>(userRepository.findByFirstName("Vira"));
    Collections.sort(usersFromDb);

    List<User> usersExpected = new ArrayList<>(Arrays.asList(expectedUser1));
    Collections.sort(usersFromDb);

    assertEquals(usersExpected, usersFromDb);
  }

  @Test
  public void testFindBySecondName() {
    List<User> usersFromDb =
            new ArrayList<>(userRepository.findBySecondName("Kuchma"));
    Collections.sort(usersFromDb);

    List<User> usersExpected = new ArrayList<>(Arrays.asList(expectedUser1));
    Collections.sort(usersFromDb);

    assertEquals(usersExpected, usersFromDb);
  }

  @Test
  public void testFindByLogin() {
    userFromDb = userRepository.findByLogin("vira");

    assertEquals(expectedUser1, userFromDb);
  }

  @Test
  public void testFindByCreatedDate() {
    List<User> usersFromDb =
            new ArrayList<>(userRepository.findByCreatedDate(new Date("24 Aug 2017 17:28:34")));
    Collections.sort(usersFromDb);

    List<User> usersExpected = new ArrayList<>(Arrays.asList(expectedUser1));
    Collections.sort(usersFromDb);

    assertEquals(usersExpected, usersFromDb);
  }

  @Test
  public void testFindByClosedDate() {
    List<User> usersFromDb =
            new ArrayList<>(userRepository.findByClosedDate(new Date("05 Sep 2017 16:08:21")));
    Collections.sort(usersFromDb);

    List<User> usersExpected = new ArrayList<>(Arrays.asList(expectedUser3));
    Collections.sort(usersFromDb);

    assertEquals(usersExpected, usersFromDb);
  }
}
