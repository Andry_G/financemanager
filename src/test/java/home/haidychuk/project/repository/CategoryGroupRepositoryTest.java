package home.haidychuk.project.repository;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import home.haidychuk.project.model.entity.Category;
import home.haidychuk.project.model.entity.CategoryGroup;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;

import org.junit.After;
import org.junit.Test;

import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.transaction.annotation.Transactional;


/**
 * Test class for CategoryGroupRepository class.
 * @author Andrii Haidychuk created on 26.11.2017
 * @version 1.0
 */
public class CategoryGroupRepositoryTest extends RepositoryTest {
  // *** Fields *** //

  private CategoryGroup categoryGroupFromDb;

  /**
   * Clearing data.
   */
  @After
  public void deInit() {
    categoryGroupFromDb = null;
  }

  @Test
  public void testFindOne() {
    categoryGroupFromDb = categoryGroupRepository.findOne(1);

    assertEquals(expectedCategoryGroup1, categoryGroupFromDb);
  }

  @Test
  public void testFindAll() {
    List<CategoryGroup> listExpected = new ArrayList<>(
            Arrays.asList(expectedCategoryGroup1, expectedCategoryGroup2));
    List<CategoryGroup> listFromDb = categoryGroupRepository.findAll();

    assertEquals(listExpected, listFromDb);
  }

  @Test
  public void testSave() {
    CategoryGroup categoryGroupNew = new CategoryGroup(expectedUser1, "Rest");
    categoryGroupNew.setId(3);

    categoryGroupRepository.save(categoryGroupNew);
    categoryGroupFromDb = categoryGroupRepository.findOne(3);

    assertEquals(categoryGroupNew, categoryGroupFromDb);
  }

  @Test
  @Transactional
  public void testDelete() {
    categoryGroupFromDb = categoryGroupRepository.findOne(1);

    // delete all Categories and their Operations for deleting CategoryGroup
    Collection<Category> categories = categoryGroupFromDb.getCategories();
    categories.forEach((cat) -> {
      cat.getOperations().forEach((op) -> operationRepository.delete(op.getId()));
      categoryRepository.delete(cat.getId());
    });

    categoryGroupRepository.delete(1);

    assertNull(categoryGroupRepository.findOne(1));
  }

  /**
   * Test operation of deleting if children entries exist.
   */
  @Test(expected = DataIntegrityViolationException.class)
  public void testDeleteConstraint() {
    categoryGroupRepository.delete(1);

    assertNull(categoryGroupRepository.findOne(1));
  }

  @Test
  public void testDeleteAll() {
    // delete all Operation, Categories and CategoryGroups for deleting CategoryGroup (id = 1)
    operationRepository.deleteAll();
    categoryRepository.deleteAll();
    categoryGroupRepository.deleteAll();

    assertTrue(categoryGroupRepository.findAll().isEmpty());
  }

  /**
   * Test operation of deleting for all categories of group if children entries exist.
   */
  @Test(expected = DataIntegrityViolationException.class)
  public void testDeleteAllConstraint() {
    categoryGroupRepository.deleteAll();

    assertNull(categoryGroupRepository.findAll());
  }

  @Test
  public void testFindByName() {
    categoryGroupFromDb = categoryGroupRepository.findByName("Meal");

    assertEquals(expectedCategoryGroup2, categoryGroupFromDb);
  }
}
