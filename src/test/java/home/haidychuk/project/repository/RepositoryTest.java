package home.haidychuk.project.repository;

import home.haidychuk.project.GeneralTest;
import home.haidychuk.project.model.repository.AccountRepository;
import home.haidychuk.project.model.repository.BudgetRepository;
import home.haidychuk.project.model.repository.CategoryGroupRepository;
import home.haidychuk.project.model.repository.CategoryRepository;
import home.haidychuk.project.model.repository.CurrencyRepository;
import home.haidychuk.project.model.repository.OperationRepository;
import home.haidychuk.project.model.repository.UserRepository;

import org.springframework.beans.factory.annotation.Autowired;

/**
 * General abstract test class for Repositories.
 * @author Andrii Haidychuk created on 17.07.2017
 * @version 1.0
 */
public abstract class RepositoryTest extends GeneralTest {
  // *** Fields *** //

  @Autowired
  protected CurrencyRepository currencyRepository;

  @Autowired
  protected AccountRepository accountRepository;

  @Autowired
  protected UserRepository userRepository;

  @Autowired
  protected CategoryGroupRepository categoryGroupRepository;

  @Autowired
  protected CategoryRepository categoryRepository;

  @Autowired
  protected OperationRepository operationRepository;

  @Autowired
  protected BudgetRepository budgetRepository;
}
