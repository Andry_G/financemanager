package home.haidychuk.project.service;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.any;
import static org.mockito.Mockito.anyInt;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;

import home.haidychuk.project.model.entity.Account;
import home.haidychuk.project.model.entity.Budget;
import home.haidychuk.project.model.entity.CategoryGroup;
import home.haidychuk.project.model.entity.User;

import home.haidychuk.project.model.exception.exceptions.NoSuchUserStatusException;
import home.haidychuk.project.model.exception.exceptions.NoSuchUserTypeException;
import home.haidychuk.project.model.exception.exceptions.RejectChangeUserStatusException;
import home.haidychuk.project.model.exception.exceptions.UserLoginExistException;
import home.haidychuk.project.model.exception.exceptions.UserNotFoundException;

import java.text.ParseException;
import java.text.SimpleDateFormat;

import java.util.ArrayList;

import java.util.Arrays;
import java.util.Collection;
import java.util.Date;
import java.util.List;

import org.junit.Test;

import org.springframework.web.bind.MethodArgumentNotValidException;

/**
 * Test class for UserService class.
 * @author Andrii Haidychuk created on 04.05.2018
 * @version 1.0
 */
public class UserServiceTest extends ServiceTest {

  //*** Test scenario for method "findById" ***//

  /* Positive test, found User is not Null */
  @Test
  public void testFindByIdNotNull() {
    //creating stub
    when(userRepositoryMock.findOne(anyInt())).thenReturn(expectedUser1);
    User user = userService.findById(1);

    assertNotNull(user);

    //verify that methods have been called
    verify(userRepositoryMock, times(1)).findOne(anyInt());
    //verify that other methods of mocked object were not called
    verifyNoMoreInteractions(userRepositoryMock);
  }

  /* Negative test, found User is Null */
  @Test(expected = UserNotFoundException.class)
  public void testFindByIdIsNull() {
    //creating stub
    when(userRepositoryMock.findOne(anyInt())).thenReturn(null);
    User user = userService.findById(1);

    //verify that methods have been called
    verify(userRepositoryMock, times(1)).findOne(anyInt());
    //verify that other methods of mocked object were not called
    verifyNoMoreInteractions(userRepositoryMock);
  }

  //*** Test scenario for method "findAll" ***//

  /* Positive test, collection of the Users is not Null */
  @Test
  public void testFindAllNotNull() {
    //creating stub
    when(userRepositoryMock.findAll()).thenReturn(Arrays.asList(expectedUser1, expectedUser2));
    List<User> users = new ArrayList<>(userService.findAll());

    assertNotNull(users);

    //verify that methods have been called
    verify(userRepositoryMock, times(1)).findAll();
    //verify that other methods of mocked object were not called
    verifyNoMoreInteractions(userRepositoryMock);
  }

  /* Negative test, collection of the Users is empty */
  @Test(expected = UserNotFoundException.class)
  public void testFindAllIsEmpty() {
    //creating stub
    when(userRepositoryMock.findAll()).thenReturn(new ArrayList<User>());
    List<User> users = new ArrayList<>(userService.findAll());

    //verify that methods have been called
    verify(userRepositoryMock, times(1)).findAll();
    //verify that other methods of mocked object were not called
    verifyNoMoreInteractions(userRepositoryMock);
  }

  //*** Test scenarios for method "create" ***//

  /* Positive test, login doesn't exist */
  @Test
  public void testCreateLoginIsNull() {
    //creating stub
    when(userRepositoryMock.findByLogin(any(String.class))).thenReturn(null);
    when(userRepositoryMock.save(any(User.class))).thenReturn(expectedUser1);
    User factUser = userService.create(expectedUser1);

    assertEquals(expectedUser1, factUser);

    //verify that methods have been called
    verify(userRepositoryMock, times(1)).findByLogin(any(String.class));
    verify(userRepositoryMock, times(1)).save(any(User.class));
    //verify that other methods of mocked object were not called
    verifyNoMoreInteractions(userRepositoryMock);
  }

  /* Negative test, login exists */
  @Test(expected = UserLoginExistException.class)
  public void testCreateLoginIsNotNull() {
    //creating stub
    when(userRepositoryMock.findByLogin(any(String.class))).thenReturn(expectedUser1);
    userService.create(expectedUser1);

    //verify that methods have been called
    verify(userRepositoryMock, times(1)).findByLogin(any(String.class));
    //verify that other methods of mocked object were not called
    verifyNoMoreInteractions(userRepositoryMock);
  }

  //*** Test scenarios for method "update" ***//

  /* Positive test, updated User exists */
  @Test
  public void testUpdateUserNotNull() {
    //creating stub
    when(userRepositoryMock.findOne(anyInt())).thenReturn(expectedUser2);
    userService.update(expectedUser2);

    //verify that methods have been called
    verify(userRepositoryMock, times(1)).findOne(anyInt());
    verify(userRepositoryMock, times(1)).save(expectedUser2);
    //verify that other methods of mocked object were not called
    verifyNoMoreInteractions(userRepositoryMock);
  }

  /* Negative test, updated User doesn't exist (is null) */
  @Test(expected = UserNotFoundException.class)
  public void testUpdateUserIsNull() {
    //creating stub
    when(userRepositoryMock.findOne(anyInt())).thenReturn(null);
    userService.update(expectedUser2);
  }

  //*** Test scenarios for method "delete" ***//

  /* Deleting user without related entities */
  @Test
  public void testDelete() throws ParseException {
    //init User
    expectedUser2.setAccounts(new ArrayList<Account>());
    expectedUser2.setCategoryGroups(new ArrayList<CategoryGroup>());
    expectedUser2.setBudgets(new ArrayList<Budget>());
    //creating stubs
    when(userRepositoryMock.findOne(anyInt())).thenReturn(expectedUser2);
    doNothing().when(userRepositoryMock).delete(anyInt());
    userService.delete(expectedUser2.getId());

    //verify that methods have been called
    verify(userRepositoryMock, times(1)).findOne(anyInt());
    verify(userRepositoryMock, times(1)).delete(anyInt());
    //verify that other methods of mocked object were not called
    verifyNoMoreInteractions(userRepositoryMock);

  }

  /* Change status for user with related entities */
  @Test
  public void testDeleteSoft() throws ParseException {
    expectedUser1.setAccounts(Arrays.asList(expectedAccount1));
    expectedUser1.setCategoryGroups(Arrays.asList(expectedCategoryGroup1));
    expectedUser1.setBudgets(Arrays.asList(expectedBudget1));

    //creating stubs
    when(userRepositoryMock.findOne(anyInt())).thenReturn(expectedUser1);
    userService.delete(expectedUser1.getId());

    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");

    assertTrue(expectedUser1.getClosedDate() != null);
    assertEquals(sdf.parse(sdf.format(new Date())),
            sdf.parse(sdf.format(expectedUser1.getClosedDate())));
    assertTrue(expectedUser1.getUserStatus().equals(User.UserStatus.CLOSED));

    //verify that methods have been called
    verify(userRepositoryMock, times(2)).findOne(anyInt());
    verify(userRepositoryMock, times(1)).save(any(User.class));
    //verify that other methods of mocked object were not called
    verifyNoMoreInteractions(userRepositoryMock);
  }

  /* User is null */
  @Test(expected = UserNotFoundException.class)
  public void testDeleteUserIsNull() {

    when(userRepositoryMock.findOne(anyInt())).thenReturn(null);
    userService.delete(expectedUser1.getId());
  }

  //*** Test scenarios for method "activateUser" ***//

  /* Activating user positive test, User is not null and his status is CLOSED */
  @Test
  public void testActivateUserPositive() {
    //creating stub
    when(userRepositoryMock.findOne(anyInt())).thenReturn(expectedUser3);
    when(userRepositoryMock.save(any(User.class))).thenReturn(expectedUser3);
    User user = userService.activateUser(expectedUser3.getId());

    assertTrue(user.getClosedDate() == null);
    assertTrue(user.getUserStatus().equals(User.UserStatus.ACTIVE));

    //verify that methods have been called
    verify(userRepositoryMock, times(1)).findOne(anyInt());
    verify(userRepositoryMock, times(1)).save(any(User.class));
    //verify that other methods of mocked object were not called
    verifyNoMoreInteractions(userRepositoryMock);
  }

  /* Activating user negative test, User is not null and his status is not CLOSED */
  @Test(expected = RejectChangeUserStatusException.class)
  public void testActivateUserNegativeUserIsNotNull() {
    //creating stub
    expectedUser1.setUserStatus(User.UserStatus.ACTIVE);
    when(userRepositoryMock.findOne(anyInt())).thenReturn(expectedUser1);
    User user = userService.activateUser(expectedUser1.getId());
  }

  /* Activating user negative test, User is null */
  @Test(expected = UserNotFoundException.class)
  public void testActivateUserNegativeUserIsNull() {
    //creating stub
    when(userRepositoryMock.findOne(anyInt())).thenReturn(null);
    User user = userService.activateUser(expectedUser1.getId());
  }

  //*** Test scenarios for method "deactivateUser" ***//

  /* Positive test, User is not null and his status is ACTIVE */
  @Test
  public void testDeactivateUserPositive() {
    //creating stub
    when(userRepositoryMock.findOne(anyInt())).thenReturn(expectedUser1);
    when(userRepositoryMock.save(any(User.class))).thenReturn(expectedUser1);
    User user = userService.deactivateUser(expectedUser1.getId());

    assertTrue(user.getClosedDate() != null);
    assertTrue(user.getUserStatus().equals(User.UserStatus.CLOSED));

    //verify that methods have been called
    verify(userRepositoryMock, times(1)).findOne(anyInt());
    verify(userRepositoryMock, times(1)).save(any(User.class));
    //verify that other methods of mocked object were not called
    verifyNoMoreInteractions(userRepositoryMock);
  }

  /* Negative test, User is not null and his status is CLOSED */
  @Test(expected = RejectChangeUserStatusException.class)
  public void testDeactivateUserNegativeUserIsNotNull() {
    //creating stub
    expectedUser1.setUserStatus(User.UserStatus.CLOSED);
    //stub
    when(userRepositoryMock.findOne(anyInt())).thenReturn(expectedUser1);
    User user = userService.deactivateUser(expectedUser1.getId());
  }

  /* Deactivating user negative test, User is null */
  @Test(expected = UserNotFoundException.class)
  public void testDeactivateUserNegativeUserIsNull() {
    //creating stub
    when(userRepositoryMock.findOne(anyInt())).thenReturn(null);
    User user = userService.deactivateUser(expectedUser1.getId());
  }

  //*** Test scenarios for method "findByUserType" ***//

  /* Positive test, collection of users is not empty */
  @Test
  public void testFindByUserType() {
    //init collection
    Collection<User> users = new ArrayList<>(Arrays.asList(expectedUser1, expectedUser3));
    //creating stub
    when(userRepositoryMock.findByUserType(any(User.UserType.class))).thenReturn(users);
    Collection<User> foundUsers = userService.findByUserType("USER");

    assertEquals(users, foundUsers);

    //verify that methods have been called
    verify(userRepositoryMock, times(1)).findByUserType(any(User.UserType.class));
    //verify that other methods of mocked object were not called
    verifyNoMoreInteractions(userRepositoryMock);
  }

  /* Negative test, collection of users is empty */
  @Test(expected = UserNotFoundException.class)
  public void testFindByUserTypeIsEmpty() {
    //init collection
    Collection<User> users = new ArrayList<>();
    //creating stub
    when(userRepositoryMock.findByUserType(any(User.UserType.class))).thenReturn(users);
    Collection<User> foundUsers = userService.findByUserType("USER");
  }

  /* Negative test, specified User type is not valid */
  @Test(expected = NoSuchUserTypeException.class)
  public void testUserTypeIsInvalid() {
    //init collection
    Collection<User> users = new ArrayList<>(Arrays.asList(expectedUser1, expectedUser3));
    //creating stub
    when(userRepositoryMock.findByUserType(any(User.UserType.class))).thenReturn(users);
    Collection<User> foundUsers = userService.findByUserType("TEST");
  }

  //*** Test scenarios for method "findByUserStatus" ***//

  /* Positive test, collection of users is not empty */
  @Test
  public void testFindByUserStatus() {
    //init collection
    Collection<User> users = new ArrayList<>(Arrays.asList(expectedUser1, expectedUser2));
    //creating stub
    when(userRepositoryMock.findByUserStatus(any(User.UserStatus.class))).thenReturn(users);
    Collection<User> foundUsers = userService.findByUserStatus("ACTIVE");

    assertEquals(users, foundUsers);

    //verify that methods have been called
    verify(userRepositoryMock, times(1)).findByUserStatus(any(User.UserStatus.class));
    //verify that other methods of mocked object were not called
    verifyNoMoreInteractions(userRepositoryMock);
  }

  /* Negative test, collection of users is not empty */
  @Test(expected = UserNotFoundException.class)
  public void testUserStatusIsEmpty() {
    //init collection
    Collection<User> users = new ArrayList<>();
    //creating stub
    when(userRepositoryMock.findByUserStatus(any(User.UserStatus.class))).thenReturn(users);
    Collection<User> foundUsers = userService.findByUserStatus("ACTIVE");
  }

  /* Negative test, User status in not valid */
  @Test(expected = NoSuchUserStatusException.class)
  public void testFindByUserStatusIsInvalid() {
    //init collection
    Collection<User> users = new ArrayList<>(Arrays.asList(expectedUser1, expectedUser2));
    //creating stub
    when(userRepositoryMock.findByUserStatus(any(User.UserStatus.class))).thenReturn(users);
    Collection<User> foundUsers = userService.findByUserStatus("TEST");
  }

  //*** Test scenarios for method "findByFirstName" ***//

  /* Positive test, collection of users is not empty */
  @Test
  public void testFindByFirstName() {
    //init collection
    Collection<User> users = new ArrayList<>(Arrays.asList(expectedUser1));
    //creating stub
    when(userRepositoryMock.findByFirstName(any(String.class))).thenReturn(users);
    Collection<User> foundUsers = userService.findByFirstName("Vira");

    assertEquals(users, foundUsers);

    //verify that methods have been called
    verify(userRepositoryMock, times(1)).findByFirstName(any(String.class));
    //verify that other methods of mocked object were not called
    verifyNoMoreInteractions(userRepositoryMock);
  }

  /* Negative test, collection of users is empty */
  @Test(expected = UserNotFoundException.class)
  public void testFindByFirstNameIsEmpty() {
    //init collection
    Collection<User> users = new ArrayList<>();
    //creating stub
    when(userRepositoryMock.findByFirstName(anyString())).thenReturn(users);
    Collection<User> foundUsers = userService.findByFirstName("Vira");
  }

  //*** Test scenarios for method "findBySecondName" ***//

  /* Positive test, collection of users is not empty */
  @Test
  public void testFindBySecondName() {
    //init collection
    Collection<User> users = new ArrayList<>(Arrays.asList(expectedUser1));
    //creating stub
    when(userRepositoryMock.findBySecondName(any(String.class))).thenReturn(users);
    Collection<User> foundUsers = userService.findBySecondName("Haidychuk");

    assertEquals(users, foundUsers);

    //verify that methods have been called
    verify(userRepositoryMock, times(1)).findBySecondName(any(String.class));
    //verify that other methods of mocked object were not called
    verifyNoMoreInteractions(userRepositoryMock);
  }

  /* Negative test, collection of users is not empty */
  @Test(expected = UserNotFoundException.class)
  public void testFindBySecondNameIsEmpty() {
    //init collection
    Collection<User> users = new ArrayList<>();
    //creating stub
    when(userRepositoryMock.findBySecondName(any(String.class))).thenReturn(users);
    Collection<User> foundUsers = userService.findBySecondName("Haidychuk");
  }

  //*** Test scenarios for method "findByCreatedDate" ***//

  /* Positive test, collection of users is not empty */
  @Test
  public void testFindByCreatedDate() {
    //init collection
    Collection<User> users = new ArrayList<>(Arrays.asList(expectedUser1));
    //creating stub
    when(userRepositoryMock.findByCreatedDate(any(Date.class))).thenReturn(users);
    Collection<User> foundUsers = userService.findByCreatedDate(new Date());

    assertEquals(users, foundUsers);

    //verify that methods have been called
    verify(userRepositoryMock, times(1)).findByCreatedDate(any(Date.class));
    //verify that other methods of mocked object were not called
    verifyNoMoreInteractions(userRepositoryMock);
  }

  /* Negative test, collection of users is empty */
  @Test(expected = UserNotFoundException.class)
  public void testFindByCreatedDateIsEmpty() {
    //init collection
    Collection<User> users = new ArrayList<>();
    //creating stub
    when(userRepositoryMock.findByCreatedDate(any(Date.class))).thenReturn(users);
    Collection<User> foundUsers = userService.findByCreatedDate(new Date());
  }

  //*** Test scenarios for method "findByCreatedDate" ***//

  /* Positive test, collection of users is not empty */
  @Test
  public void testFindByClosedDate() {
    //init collection
    Collection<User> users = new ArrayList<>(Arrays.asList(expectedUser3));
    //creating stub
    when(userRepositoryMock.findByClosedDate(any(Date.class))).thenReturn(users);
    Collection<User> foundUsers = userService.findByClosedDate(new Date());

    assertEquals(users, foundUsers);

    //verify that methods have been called
    verify(userRepositoryMock, times(1)).findByClosedDate(any(Date.class));
    //verify that other methods of mocked object were not called
    verifyNoMoreInteractions(userRepositoryMock);
  }

  /* Negative test, collection of users is empty */
  @Test(expected = UserNotFoundException.class)
  public void testFindByClosedDateisEmpty() {
    //init collection
    Collection<User> users = new ArrayList<>();
    //creating stub
    when(userRepositoryMock.findByClosedDate(any(Date.class))).thenReturn(users);
    Collection<User> foundUsers = userService.findByClosedDate(new Date());
  }
}
