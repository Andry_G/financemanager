package home.haidychuk.project.service;

import home.haidychuk.project.GeneralTest;

import home.haidychuk.project.model.repository.UserRepository;
import home.haidychuk.project.model.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;

/**
 * General abstract test class for Services.
 * @author Andrii Haidychuk created on 04.05.2018
 * @version 1.0
 */
public abstract class ServiceTest extends GeneralTest {
  // *** Fields *** //

  @MockBean
  protected UserRepository userRepositoryMock;

  @Autowired
  protected UserService userService;
}
