-- Test DB
SELECT * FROM testfm.User;
SELECT * FROM testfm.Currency;
SELECT * FROM testfm.Account;
SELECT * FROM testfm.CategoryGroup;
SELECT * FROM testfm.Category;
SELECT * FROM testfm.Operation;
SELECT * FROM testfm.Budget;
SELECT * FROM testfm.BudgetAccount;
SELECT * FROM testfm.BudgetCategoryGroup;
SELECT * FROM testfm.BudgetCategory;

INSERT INTO testfm.budgetaccount (budget_id, account_id) VALUES (1, 1);
DELETE FROM testfm.budgetaccount WHERE budget_id = 1;
DELETE FROM testfm.budget WHERE id = 1;
DELETE FROM testfm.operation;
DELETE FROM testfm.account WHERE id = 1;

DELETE FROM operation;


-- Production DB
SELECT * FROM finance_manager.User;
SELECT * FROM finance_manager.Currency;
SELECT * FROM finance_manager.Account;
SELECT * FROM finance_manager.CategoryGroup;
SELECT * FROM finance_manager.Category;
SELECT * FROM finance_manager.Operation;
SELECT * FROM finance_manager.Budget;
SELECT * FROM finance_manager.BudgetAccount;
SELECT * FROM finance_manager.BudgetCategoryGroup;
SELECT * FROM finance_manager.BudgetCategory;    