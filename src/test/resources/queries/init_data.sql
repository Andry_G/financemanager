 USE testfm;
-- USE finance_manager;

-- init table User
INSERT INTO USER(user_type, user_status, first_name, second_name, login, password, created_date)
  VALUES ('USER', 'ACTIVE', 'Vira', 'Kuchma', 'vira', 'virakuch', '2017-08-24 17:28:34');
INSERT INTO USER(user_type, user_status, first_name, second_name, login, password, created_date)
  VALUES ('ADMIN', 'ACTIVE', 'Andrii', 'Haidychuk', 'andrii', 'andrhaid', '2017-07-30 13:16:42');
INSERT INTO USER(user_type, user_status, first_name, second_name, login, password, created_date, closed_date)
  VALUES ('USER', 'CLOSED', 'Taras', 'Haidychuk', 'taras', 'tarhaid', '2017-08-10 09:38:51', '2017-09-05 16:08:21');

-- init table Currency
INSERT INTO Currency (short_title, title) VALUES ('UAH', 'Гривня');
INSERT INTO Currency (short_title, title) VALUES ('USD', 'Долар США');
INSERT INTO Currency (short_title, title) VALUES ('EUR', 'Євро');

-- init table Account
INSERT INTO Account (user_id, currency_id, name, balance, total_included) VALUES (1, 1, 'Готівка,грн.', 1000, TRUE);
INSERT INTO Account (user_id, currency_id, name, balance, total_included) VALUES (1, 2, 'Готівка,дол.', 500, TRUE);
INSERT INTO Account (user_id, currency_id, name, balance, total_included) VALUES (1, 3, 'Готівка,євро.', 700, TRUE);
INSERT INTO Account (user_id, currency_id, name, balance, total_included) VALUES (3, 1, 'Депозит,грн.', 15000, TRUE);

-- init table CategoryGroup
INSERT INTO Categorygroup (user_id, name) VALUES (1, 'Car');
INSERT INTO Categorygroup (user_id, name) VALUES (1, 'Meal');

-- init table Category
INSERT INTO Category (group_id, name, category_type) VALUES (1, 'Fuel', 'EXPENSE');
INSERT INTO Category (group_id, name, category_type) VALUES (1, 'Parking', 'EXPENSE');
INSERT INTO Category (group_id, name, category_type) VALUES (2, 'Breakfast', 'EXPENSE');
INSERT INTO Category (group_id, name, category_type) VALUES (2, 'Cafe', 'EXPENSE');

-- init table Operation
INSERT INTO Operation (category_id, source_id, destination_id, operation_type, date, amount, rate)
  VALUES (1, 1, NULL, 0, '2017-11-25', 150, 1);
INSERT INTO Operation (category_id, source_id, destination_id, operation_type, date, amount, rate, comment)
  VALUES (2, NUll, 1, 1, '2017-12-01', 10, 1, 'test income operation');

-- init table Budget
SET @start_date:= date_add(current_date(), interval -day(current_date()) + 1 DAY);
SET @end_date:= LAST_DAY(current_date());
INSERT INTO Budget (user_id, currency_id, name, amount, budget_type, time_range, by_date_range, start_date, end_date)
  VALUES (1, 1, 'CarBudget', 500, 0, 2, FALSE , @start_date, @end_date);

-- init table BudgetAccount
INSERT INTO BudgetAccount (budget_id, account_id) VALUES (1, 1);

-- init table BudgetCategoryGroup
INSERT INTO BudgetCategoryGroup (budget_id, category_group_id) VALUES (1, 1);

-- init table BudgetCategory
INSERT INTO BudgetCategory (budget_id, category_id) VALUES (1, 1);
INSERT INTO BudgetCategory (budget_id, category_id) VALUES (1, 2);