-- create User for Production Database
CREATE USER finManagerUser IDENTIFIED WITH mysql_native_password BY 'finmanager_access';
-- grant access for finManagerUser
GRANT INSERT ON finance_manager.* TO finManagerUser;
GRANT SELECT ON finance_manager.* TO finManagerUser;
GRANT UPDATE ON finance_manager.* TO finManagerUser;
GRANT DELETE ON finance_manager.* TO finManagerUser;
GRANT ALTER ON finance_manager.* TO finManagerUser;
-- drop User
DROP USER finManagerUser;


-- create User for Test Database
CREATE USER 'testFmUser'@'localhost' IDENTIFIED WITH mysql_native_password BY 'testfmuser_access';
-- grant access for testFmUser
GRANT INSERT ON testfm.* TO 'testFmUser'@'localhost';
GRANT SELECT ON testfm.* TO 'testFmUser'@'localhost';
GRANT UPDATE ON testfm.* TO 'testFmUser'@'localhost';
GRANT DELETE ON testfm.* TO 'testFmUser'@'localhost';
GRANT ALTER ON testfm.* TO 'testFmUser'@'localhost';
-- drop User
DROP USER 'testFmUser'@'localhost';