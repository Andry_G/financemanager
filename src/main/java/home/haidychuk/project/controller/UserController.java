package home.haidychuk.project.controller;

import home.haidychuk.project.model.entity.User;
import home.haidychuk.project.model.service.UserService;

import java.text.ParseException;

import java.util.Arrays;
import java.util.Collection;
import java.util.Date;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;

import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 * Rest Controller for User.
 * @author Andrii Haidychuk created on 02.04.2018
 * @version 1.0
 */
@RestController
@RequestMapping(value = "/user")
public class UserController {
  //TODO add Security level based on Auth 2.0
  @Autowired
  private UserService userService;

  private HttpHeaders httpHeaders = new HttpHeaders();

  //fill HttpHeaders
  {
    httpHeaders.put(HttpHeaders.CONTENT_TYPE,
            Arrays.asList(MediaType.APPLICATION_JSON_UTF8.toString()));
  }

  /**
   * Get User by Id.
   * @return found User
   */
  @RequestMapping(value = "/{id}", method = RequestMethod.GET)
  public ResponseEntity<User> findById(
          @PathVariable("id")
          Integer id) {

    //Search User
    User foundUser = userService.findById(id);

    return new ResponseEntity<User>(foundUser, httpHeaders, HttpStatus.OK);
  }

  /**
   * Get Collection of all Users.
   * @return collection of the all Users.
   */
  @RequestMapping(method = RequestMethod.GET)
  public ResponseEntity<Collection<User>> findAll() {
    //Search users
    Collection<User> users = userService.findAll();

    return new ResponseEntity<Collection<User>>(users, httpHeaders, HttpStatus.OK);
  }

  /**
   * Create new User.
   * @param user new User
   * @return created User
   */
  @RequestMapping(method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
  public ResponseEntity<User> create(
          @RequestBody
          @Valid
          User user) {

    //Search such User
    User newUser = userService.create(user);

    return new ResponseEntity<User>(newUser, httpHeaders, HttpStatus.CREATED);
  }

  /**
   * Update existing User.
   * @param user updated user
   * @return updated User
   */
  @RequestMapping(method = RequestMethod.PUT, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
  public ResponseEntity<User> update(
          @RequestBody
          @Valid
          User user) {

    //Search origin User
    User updatedUser = userService.update(user);

    return new ResponseEntity<User>(user, httpHeaders, HttpStatus.OK);
  }

  /**
   * Delete User by Id.
   * @param id User's Id
   * @return deleted User
   */
  @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
  public ResponseEntity<User> delete(
          @PathVariable("id")
          Integer id) {

    User user = userService.delete(id);

    return new ResponseEntity<User>(user, httpHeaders, HttpStatus.OK);
  }

  /**
   * Activating closed User.
   * @return activate User
   */
  @RequestMapping(value = "/activate/{id}", method = RequestMethod.POST)
  public ResponseEntity<User> activateUser(
          @PathVariable("id")
          int id) {

    User user = userService.activateUser(id);

    return new ResponseEntity<User>(user, httpHeaders, HttpStatus.OK);
  }

  /**
   * Get Collection of Users that have specified UserType.
   * @param requestedType User's type
   * @return Collection of Users with specified type
   */
  @RequestMapping(value = "/type/{userType}", method = RequestMethod.GET)
  public ResponseEntity<Collection<User>> findByUserType(
          @PathVariable("userType")
          String requestedType) {

    Collection<User> users = userService.findByUserType(requestedType);

    return new ResponseEntity<Collection<User>>(users, httpHeaders, HttpStatus.OK);
  }

  /**
   * Get Collection of Users that have specified UserStatus.
   * @param requestedStatus User's requestedStatus
   * @return Collection of Users with specified requestedStatus
   */
  @RequestMapping(value = "/status/{userStatus}", method = RequestMethod.GET)
  public ResponseEntity<Collection<User>> findByUserStatus(
          @PathVariable("userStatus")
          String requestedStatus) {

    Collection<User> users = userService.findByUserStatus(requestedStatus);

    return new ResponseEntity<Collection<User>>(users, httpHeaders, HttpStatus.OK);
  }

  /**
   * Get Collection of Users that have specified FirstName.
   * @param firstName User's FirstName
   * @return Collection of Users with specified FirstName
   */
  @RequestMapping(value = "/firstName/{firstName}", method = RequestMethod.GET)
  public ResponseEntity<Collection<User>> findByFirstName(
          @PathVariable("firstName")
          String firstName) {

    Collection<User> users = userService.findByFirstName(firstName);

    return new ResponseEntity<Collection<User>>(users, httpHeaders, HttpStatus.OK);
  }

  /**
   * Get Collection of Users that have specified SecondName.
   * @param secondName User's SecondName
   * @return Collection of Users with specified SecondName
   */
  @RequestMapping(value = "/secondName/{secondName}", method = RequestMethod.GET)
  public ResponseEntity<Collection<User>> findBySecondName(
          @PathVariable("secondName")
          String secondName) {

    Collection<User> users = userService.findBySecondName(secondName);

    return new ResponseEntity<Collection<User>>(users, httpHeaders, HttpStatus.OK);
  }

  /**
   * Get Collection of Users that have specified CreatedDate.
   * Date should be presented in milliseconds
   * @param createdDate User's CreatedDate
   * @return Collection of Users with specified CreatedDate
   */
  @RequestMapping(value = "/createdDate/{createdDate}", method = RequestMethod.GET)
  public ResponseEntity<Collection<User>> findByCreatedDate(
          @PathVariable("createdDate")
          String createdDate) throws ParseException {

    Date date = new Date(Long.valueOf(createdDate));
    Collection<User> users = userService.findByCreatedDate(date);

    return new ResponseEntity<Collection<User>>(users, httpHeaders, HttpStatus.OK);
  }

  /**
   * Get Collection of Users that have specified ClosedDate.
   * Date should be presented in milliseconds
   * @param closedDate User's ClosedDate
   * @return Collection of Users with specified ClosedDate
   */
  @RequestMapping(value = "/closedDate/{closedDate}", method = RequestMethod.GET)
  public ResponseEntity<Collection<User>> findByClosedDate(
          @PathVariable("closedDate")
          Long closedDate) throws ParseException {

    Date date = new Date(closedDate);
    Collection<User> users = userService.findByClosedDate(date);

    if (users.isEmpty()) {
      return new ResponseEntity<Collection<User>>(HttpStatus.NOT_FOUND);
    }

    return new ResponseEntity<Collection<User>>(users, httpHeaders, HttpStatus.OK);
  }
}
