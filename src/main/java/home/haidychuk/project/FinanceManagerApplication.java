package home.haidychuk.project;

import com.ulisesbocchio.jasyptspringboot.annotation.EnableEncryptableProperties;
import org.jasypt.encryption.StringEncryptor;
import org.jasypt.encryption.pbe.PooledPBEStringEncryptor;
import org.jasypt.encryption.pbe.config.SimpleStringPBEConfig;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

@SpringBootApplication
@EnableEncryptableProperties
public class FinanceManagerApplication {

  public static void main(String[] args) {
    SpringApplication.run(FinanceManagerApplication.class, args);
  }

  /**
   * Configuration custom encryption for property files.
   * @return encrypted value of property
   */
  @Bean(name = "encryptorBean")
  public static StringEncryptor stringEncryptor() {
    SimpleStringPBEConfig config = new SimpleStringPBEConfig();
    config.setPassword("master_password");
    config.setAlgorithm("PBEWithMD5AndDES");
    config.setKeyObtentionIterations("1000");
    config.setPoolSize("1");
    config.setProviderName("SunJCE");
    config.setSaltGeneratorClassName("org.jasypt.salt.RandomSaltGenerator");
    config.setStringOutputType("base64");

    PooledPBEStringEncryptor encryptor = new PooledPBEStringEncryptor();
    encryptor.setConfig(config);
    return encryptor;
  }

}
