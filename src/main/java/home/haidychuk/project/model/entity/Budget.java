package home.haidychuk.project.model.entity;

import static home.haidychuk.project.model.util.DateUtil.dateWithRequestedDay;

import home.haidychuk.project.model.util.DateUtil;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Collection;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

/**
 * Class for reflect table Budget.
 * @author Andrii Haidychuk created date 18.10.2017
 * @version 1.0
 */
@Entity
@Table(name = "Budget")
public class Budget implements Serializable, Comparable<Budget> {
  // *** Fields *** //

  @Id
  @GeneratedValue
  @Column(name = "id", nullable = false)
  private Integer id;

  /**
   * Relationship to table User.
   */
  @ManyToOne(fetch = FetchType.EAGER)
  @JoinColumn(name = "user_id", nullable = false)
  private User user;

  /**
   * Relationship to table Currency.
   */
  @ManyToOne(fetch = FetchType.EAGER)
  @JoinColumn(name = "currency_id", nullable = false)
  private Currency currency;

  @Size(min = 2, max = 25)
  @Pattern(regexp = "^[\\D\\S].+")
  @Column(name = "name", length = 25, nullable = false)
  private String name;

  @Column(name = "amount", nullable = false)
  private BigDecimal amount;

  @Enumerated(EnumType.ORDINAL)
  @Column(name = "budget_type", length = 1, nullable = false)
  private BudgetType budgetType;

  @Enumerated(EnumType.ORDINAL)
  @Column(name = "time_range", length = 1)
  private Range timeRange;

  @Column(name = "by_date_range", nullable = false)
  private Boolean byDateRange;

  @Temporal(TemporalType.DATE)
  @Column(name = "start_date", nullable = false)
  private Date startDate;

  @Temporal(TemporalType.DATE)
  @Column(name = "end_date", nullable = false)
  private Date endDate;

  /**
   * Relationship to table CategoryGroup.
   */
  @ManyToMany(fetch = FetchType.LAZY)
  @JoinTable(name = "Budgetcategorygroup",
             joinColumns = @JoinColumn(name = "budget_id", nullable = false),
             inverseJoinColumns = @JoinColumn(name = "category_group_id", nullable = false)
  )
  private Collection<CategoryGroup> categoryGroups;

  /**
   * Relationship to table Category.
   */
  @ManyToMany(fetch = FetchType.LAZY)
  @JoinTable(name = "Budgetcategory",
             joinColumns = @JoinColumn(name = "budget_id", nullable = false),
             inverseJoinColumns = @JoinColumn(name = "category_id", nullable = false)
  )
  private Collection<Category> categories;

  /**
   * Relationship to table Account.
   */
  @ManyToMany(fetch = FetchType.LAZY)
  @JoinTable(name = "Budgetaccount",
             joinColumns = @JoinColumn(name = "budget_id", nullable = false),
             inverseJoinColumns = @JoinColumn(name = "account_id", nullable = false)
  )
  private Collection<Account> accounts;

  /**
   * Set on default values for budgetType, timeRange, byDateRange, startDate, endDate.
   */
  @PrePersist
  public void prePersistValues() {
    if (budgetType == null) {
      budgetType = BudgetType.EXPENSE;
    }

    if (byDateRange == null) {
      byDateRange = false;
    }

    if (timeRange == null  && !byDateRange) {
      timeRange = Range.CURRENT_MONTH;
    }

    if (startDate == null && endDate == null) {
      startDate = dateWithRequestedDay(DateUtil.Day.FIRST_DAY_OF_MONTH);
      endDate = dateWithRequestedDay(DateUtil.Day.LAST_DAY_OF_MONTH);
    }
  }

  /**
   * Set on values for startDate and endDate before updating.
   */
  @PreUpdate
  public void preUpdateValues() {
    if (byDateRange) {
      timeRange = null;

    } else {

      switch (timeRange) {
        case CURRENT_WEEK:
          //calculation the start day of the current week (Monday, value = 2)
          startDate = dateWithRequestedDay(DateUtil.Day.FIRST_DAY_OF_WEEK);

          //calculation the last day of the current week
          endDate = dateWithRequestedDay(DateUtil.Day.LAST_DAY_OF_WEEK);
          break;

        case LAST_WEEK:
          // calculation the start day of the last week (Monday, value = 2)
          startDate = dateWithRequestedDay(DateUtil.Day.FIRST_DAY_OF_LAST_WEEK);

          //calculation the last day of the last week
          endDate = dateWithRequestedDay(DateUtil.Day.LAST_DAY_OF_LAST_WEEK);
          break;

        case CURRENT_MONTH:
          //calculation the start day of the current month
          startDate = dateWithRequestedDay(DateUtil.Day.FIRST_DAY_OF_MONTH);

          //calculation the last day of the current month
          endDate = dateWithRequestedDay(DateUtil.Day.LAST_DAY_OF_MONTH);
          break;

        case LAST_MONTH:
          //calculation the start day of the last month
          startDate = dateWithRequestedDay(DateUtil.Day.FIRST_DAY_OF_LAST_MONTH);

          //calculation the last day of the last month
          endDate = dateWithRequestedDay(DateUtil.Day.LAST_DAY_OF_LAST_MONTH);
          break;

        case CURRENT_YEAR:
          //calculation the start day of the current year
          startDate = dateWithRequestedDay(DateUtil.Day.FIRST_DAY_OF_YEAR);

          //calculation the last day of the current year
          endDate = dateWithRequestedDay(DateUtil.Day.LAST_DAY_OF_YEAR);
          break;

        case LAST_YEAR:
          //calculation the start day of the last year
          startDate = dateWithRequestedDay(DateUtil.Day.FIRST_DAY_OF_LAST_YEAR);

          //calculation the last day of the last year
          endDate = dateWithRequestedDay(DateUtil.Day.LAST_DAY_OF_LAST_YEAR);
          break;

        default:
      }
    }
  }

  // *** Constructors *** //

  /**
   * Default constructor.
   */
  public Budget() {
  }

  /**
   * Constructor for creating new Budget.
   * @param user owner of the budget
   * @param currency currency of Budget
   * @param name name of the Budget
   * @param amount amount of the Budget
   * @param budgetType BudgetType value
   * @param timeRange Range value
   */
  public Budget(User user, Currency currency, String name, BigDecimal amount,
                BudgetType budgetType, Range timeRange) {
    this.user = user;
    this.currency = currency;
    this.name = name;
    this.amount = amount;
    this.budgetType = budgetType;
    this.timeRange = timeRange;
  }

  /**
   * Constructor for creating new Budget.
   * @param user owner of the budget
   * @param currency currency of Budget
   * @param name name of the Budget
   * @param amount amount of the Budget
   */
  public Budget(User user, Currency currency, String name, BigDecimal amount) {
    this.user = user;
    this.currency = currency;
    this.name = name;
    this.amount = amount;
  }

  // *** Setters *** //

  public void setId(Integer id) {
    this.id = id;
  }

  public void setUser(User user) {
    this.user = user;
  }

  public void setCurrency(Currency currency) {
    this.currency = currency;
  }

  public void setName(String name) {
    this.name = name;
  }

  public void setAmount(BigDecimal amount) {
    this.amount = amount;
  }

  public void setBudgetType(BudgetType budgetType) {
    this.budgetType = budgetType;
  }

  public void setTimeRange(Range timeRange) {
    this.timeRange = timeRange;
  }

  public void setByDateRange(Boolean byDateRange) {
    this.byDateRange = byDateRange;
  }

  public void setStartDate(Date startDate) {
    this.startDate = startDate;
  }

  public void setEndDate(Date endDate) {
    this.endDate = endDate;
  }

  public void setCategoryGroups(Collection<CategoryGroup> categoryGroups) {
    this.categoryGroups = categoryGroups;
  }

  public void setCategories(Collection<Category> categories) {
    this.categories = categories;
  }

  public void setAccounts(Collection<Account> accounts) {
    this.accounts = accounts;
  }

  // *** Getters *** //

  public Integer getId() {
    return id;
  }

  public User getUser() {
    return user;
  }

  public Currency getCurrency() {
    return currency;
  }

  public String getName() {
    return name;
  }

  public BigDecimal getAmount() {
    return amount;
  }

  public BudgetType getBudgetType() {
    return budgetType;
  }

  public Range getTimeRange() {
    return timeRange;
  }

  public Boolean getByDateRange() {
    return byDateRange;
  }

  public Date getStartDate() {
    return startDate;
  }

  public Date getEndDate() {
    return endDate;
  }

  public Collection<CategoryGroup> getCategoryGroups() {
    return categoryGroups;
  }

  public Collection<Category> getCategories() {
    return categories;
  }

  public Collection<Account> getAccounts() {
    return accounts;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }

    Budget budget = (Budget) o;

    if (id != null ? !id.equals(budget.id) : budget.id != null) {
      return false;
    }
    if (name != null ? !name.equals(budget.name) : budget.name != null) {
      return false;
    }
    if (amount != null ? !amount.equals(budget.amount) : budget.amount != null) {
      return false;
    }
    if (budgetType != budget.budgetType) {
      return false;
    }
    if (timeRange != budget.timeRange) {
      return false;
    }
    if (byDateRange != null
            ? !byDateRange.equals(budget.byDateRange) : budget.byDateRange != null) {
      return false;
    }
    if (startDate != null ? !startDate.equals(budget.startDate) : budget.startDate != null) {
      return false;
    }
    return endDate != null ? endDate.equals(budget.endDate) : budget.endDate == null;
  }

  @Override
  public int hashCode() {
    int result = id != null ? id.hashCode() : 0;
    result = 31 * result + (name != null ? name.hashCode() : 0);
    result = 31 * result + (amount != null ? amount.hashCode() : 0);
    result = 31 * result + (budgetType != null ? budgetType.hashCode() : 0);
    result = 31 * result + (timeRange != null ? timeRange.hashCode() : 0);
    result = 31 * result + (byDateRange != null ? byDateRange.hashCode() : 0);
    result = 31 * result + (startDate != null ? startDate.hashCode() : 0);
    result = 31 * result + (endDate != null ? endDate.hashCode() : 0);
    return result;
  }

  @Override
  public int compareTo(Budget budget) {
    return this.id > budget.id ? 1 : this.id < budget.id ? -1 : 0;
  }

  @Override
  public String toString() {
    return "Budget{"
            + "hashcode=" + hashCode()
            + ", id=" + id
            + ", name='" + name + '\''
            + ", amount=" + amount
            + ", budgetType=" + budgetType
            + ", timeRange=" + timeRange
            + ", byDateRange=" + byDateRange
            + ", startDate=" + startDate
            + ", endDate=" + endDate
            + '}';
  }

  // *** Enums *** //

  /**
   * Budget types.
   */
  public enum BudgetType {
    EXPENSE, PROFITABLE, UNIVERSAL
  }

  /**
   * Budget time ranges.
   */
  public enum Range {
    CURRENT_WEEK, LAST_WEEK, CURRENT_MONTH, LAST_MONTH, CURRENT_YEAR, LAST_YEAR
  }
}
