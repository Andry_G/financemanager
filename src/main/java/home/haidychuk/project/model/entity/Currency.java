package home.haidychuk.project.model.entity;

import java.io.Serializable;
import java.util.Collection;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

/**
 * Class for reflect table Currency.
 * @author Andrii Haidychuk created on 11.07.2017
 * @version 1.0
 */
@Entity
@Table(name = "Currency")
public class Currency implements Serializable, Comparable<Currency> {
  // *** Fields *** //

  @Id
  @GeneratedValue
  @Column(name = "id", nullable = false)
  private Integer id;

  @Size(min = 3, max = 5)
  @Pattern(regexp = "^[\\D\\S].+$")
  @Column(name = "short_title", length = 5, nullable = false, unique = true)
  private String shortTitle;

  @Size(min = 3, max = 30)
  @Pattern(regexp = "^[\\D\\S].+$")
  @Column(name = "title", length = 30, nullable = false, unique = true)
  private String title;

  /**
   * Relationship to the table Account.
   */
  @OneToMany(mappedBy = "currency", fetch = FetchType.LAZY,
             cascade = {CascadeType.PERSIST, CascadeType.REFRESH,
                        CascadeType.MERGE, CascadeType.DETACH})
  private Collection<Account> accounts;

  /**
   * Relationship to the table Budget.
   */
  @OneToMany(mappedBy = "currency", fetch = FetchType.LAZY,
             cascade = {CascadeType.PERSIST, CascadeType.REFRESH,
                        CascadeType.MERGE, CascadeType.DETACH})
  private Collection<Budget> budgets;

  // *** Constructors *** //

  /**
   * Default constructor.
   */
  public Currency() {
  }

  /**
   * Constructor for creating new Currency.
   * @param shortTitle Currency's short title
   * @param title Currency's title
   */
  public Currency(String shortTitle, String title) {
    this.shortTitle = shortTitle;
    this.title = title;
  }

  // *** Setters *** //

  public void setId(Integer id) {
    this.id = id;
  }

  public void setShortTitle(String shortTitle) {
    this.shortTitle = shortTitle;
  }

  public void setTitle(String title) {
    this.title = title;
  }

  public void setAccounts(Collection<Account> accounts) {
    this.accounts = accounts;
  }

  public void setBudgets(Collection<Budget> budgets) {
    this.budgets = budgets;
  }

  // *** Getters *** //

  public Integer getId() {
    return id;
  }

  public String getShortTitle() {
    return shortTitle;
  }

  public String getTitle() {
    return title;
  }

  public Collection<Account> getAccounts() {
    return accounts;
  }

  public Collection<Budget> getBudgets() {
    return budgets;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }

    Currency currency = (Currency) o;

    if (id != null ? !id.equals(currency.id) : currency.id != null) {
      return false;
    }
    if (shortTitle != null ? !shortTitle.equals(currency.shortTitle)
            : currency.shortTitle != null) {
      return false;
    }
    return title != null ? title.equals(currency.title) : currency.title == null;
  }

  @Override
  public int hashCode() {
    int result = id != null ? id.hashCode() : 0;
    result = 31 * result + (shortTitle != null ? shortTitle.hashCode() : 0);
    result = 31 * result + (title != null ? title.hashCode() : 0);
    return result;
  }

  @Override
  public int compareTo(Currency currency) {
    return this.id > currency.id ? 1 : this.id < currency.id ? -1 : 0;
  }

  @Override
  public String toString() {
    return "Currency{"
            + "hasCode=" + hashCode()
            + ", id=" + id
            + ", shortTitle='" + shortTitle + '\''
            + ", title='" + title + '\''
            + '}';
  }
}
