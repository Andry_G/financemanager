package home.haidychuk.project.model.entity;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;

import java.io.Serializable;
import java.util.Collection;
import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.PrePersist;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

/**
 * Class for reflect table User.
 * @author Andrii Haidychuk created on 07.07.2017
 * @version 1.0
 */
@Entity
@Table(name = "User")
public class User implements Serializable, Comparable<User> {
  // *** Fields *** //

  @Id
  @GeneratedValue
  @Column(name = "id", nullable = false)
  private Integer id;

  @NotNull
  @Enumerated(EnumType.STRING)
  @Column(name = "user_type", length = 20, nullable = false)
  private UserType userType;

  @NotNull
  @Enumerated(EnumType.STRING)
  @Column(name = "user_status", length = 20, nullable = false)
  private UserStatus userStatus;

  @NotNull
  @Size(min = 2, max = 30)
  @Pattern(regexp = "^[\\D\\S].+")
  @Column(name = "first_name", length = 30, nullable = false)
  private String firstName;

  @NotNull
  @Size(min = 2, max = 30)
  @Pattern(regexp = "^[\\D\\S].+")
  @Column(name = "second_name", length = 30, nullable = false)
  private String secondName;

  @NotNull
  @Size(min = 4, max = 20)
  @Pattern(regexp = "^[\\S].*")
  @Column(name = "login", length = 20, nullable = false, unique = true)
  private String login;

  @NotNull
  @Column(name = "password", length = 50, nullable = false)
  // TODO need to make up transfer mechanism for password from frontend to backend
  private String password;

  @Temporal(TemporalType.TIMESTAMP)
  @Column(name = "created_date", nullable = false)
  @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss")
  private Date createdDate;

  @Temporal(TemporalType.TIMESTAMP)
  @Column(name = "closed_date")
  @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss")
  private Date closedDate;

  /**
   * Relationship to the table Account.
   */
  @OneToMany(mappedBy = "user", fetch = FetchType.LAZY,
             cascade = {CascadeType.PERSIST, CascadeType.REFRESH,
                        CascadeType.MERGE, CascadeType.DETACH})
  private Collection<Account> accounts;

  /**
   * Relationship to the table CategoryGroup.
   */
  @OneToMany(mappedBy = "user", fetch = FetchType.LAZY,
             cascade = {CascadeType.PERSIST, CascadeType.REFRESH,
                        CascadeType.MERGE, CascadeType.DETACH})
  private Collection<CategoryGroup> categoryGroups;

  /**
   * Relationship to the table Budget.
   */
  @OneToMany(mappedBy = "user", fetch = FetchType.LAZY,
             cascade = {CascadeType.PERSIST, CascadeType.REFRESH,
                        CascadeType.MERGE, CascadeType.DETACH})
  private Collection<Budget> budgets;

  /**
   * Set on default value for User's created date.
   */
  @PrePersist
  public void prePersistValues() {
    if (createdDate == null) {
      createdDate = new Date();
    }
  }

  // *** Constructors *** //

  /**
   * Default constructor.
   */
  public User() {
  }

  /**
   * Constructor for creating new User.
   * @param userType User's type
   * @param userStatus User's status
   * @param firstName User's first name
   * @param secondName User's second name
   * @param login User's login
   * @param password User's password
   */
  public User(UserType userType, UserStatus userStatus, String firstName,
              String secondName, String login, String password) {
    this.userType = userType;
    this.userStatus = userStatus;
    this.firstName = firstName;
    this.secondName = secondName;
    this.login = login;
    this.password = password;
  }

  /**
   * Constructor for active User.
   * @param userType User's type
   * @param userStatus User's status
   * @param firstName User's first name
   * @param secondName User's second name
   * @param login User's login
   * @param password User's password
   * @param createdDate User's created date
   */
  public User(UserType userType, UserStatus userStatus, String firstName,
              String secondName, String login, String password, Date createdDate) {
    this.userType = userType;
    this.userStatus = userStatus;
    this.firstName = firstName;
    this.secondName = secondName;
    this.login = login;
    this.password = password;
    this.createdDate = createdDate;
  }

  /**
   * Constructor for closed User.
   * @param userType User's type
   * @param userStatus User's status
   * @param firstName User's first name
   * @param secondName User's second name
   * @param login User's login
   * @param password User's password
   * @param createdDate User's created date
   * @param closedDate User's closed date
   */
  public User(UserType userType, UserStatus userStatus, String firstName, String secondName,
              String login, String password, Date createdDate, Date closedDate) {
    this.userType = userType;
    this.userStatus = userStatus;
    this.firstName = firstName;
    this.secondName = secondName;
    this.login = login;
    this.password = password;
    this.createdDate = createdDate;
    this.closedDate = closedDate;
  }

  // *** Setters *** //

  public void setId(Integer id) {
    this.id = id;
  }

  public void setUserType(UserType userType) {
    this.userType = userType;
  }

  public void setUserStatus(UserStatus userStatus) {
    this.userStatus = userStatus;
  }

  public void setFirstName(String firstName) {
    this.firstName = firstName;
  }

  public void setSecondName(String secondName) {
    this.secondName = secondName;
  }

  public void setLogin(String login) {
    this.login = login;
  }

  public void setPassword(String password) {
    this.password = password;
  }

  public void setCreatedDate(Date createdDate) {
    this.createdDate = createdDate;
  }

  public void setClosedDate(Date closedDate) {
    this.closedDate = closedDate;
  }

  public void setAccounts(Collection<Account> accounts) {
    this.accounts = accounts;
  }

  public void setCategoryGroups(Collection<CategoryGroup> categoryGroups) {
    this.categoryGroups = categoryGroups;
  }

  public void setBudgets(Collection<Budget> budgets) {
    this.budgets = budgets;
  }

  // *** Getters *** //

  public Integer getId() {
    return id;
  }

  public UserType getUserType() {
    return userType;
  }

  public UserStatus getUserStatus() {
    return userStatus;
  }

  public String getFirstName() {
    return firstName;
  }

  public String getSecondName() {
    return secondName;
  }

  public String getLogin() {
    return login;
  }

  //@JsonIgnore
  public String getPassword() {
    return password;
  }

  public Date getCreatedDate() {
    return createdDate;
  }

  public Date getClosedDate() {
    return closedDate;
  }

  @JsonIgnore
  public Collection<Account> getAccounts() {
    return accounts;
  }

  @JsonIgnore
  public Collection<CategoryGroup> getCategoryGroups() {
    return categoryGroups;
  }

  @JsonIgnore
  public Collection<Budget> getBudgets() {
    return budgets;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }

    User user = (User) o;

    if (id != null ? !id.equals(user.id) : user.id != null) {
      return false;
    }
    if (userType != user.userType) {
      return false;
    }
    if (userStatus != user.userStatus) {
      return false;
    }
    if (firstName != null ? !firstName.equals(user.firstName) : user.firstName != null) {
      return false;
    }
    if (secondName != null ? !secondName.equals(user.secondName) : user.secondName != null) {
      return false;
    }
    if (login != null ? !login.equals(user.login) : user.login != null) {
      return false;
    }
    if (createdDate != null ? !createdDate.equals(user.createdDate) : user.createdDate != null) {
      return false;
    }
    return closedDate != null ? closedDate.equals(user.closedDate) : user.closedDate == null;
  }

  @Override
  public int hashCode() {
    int result = id != null ? id.hashCode() : 0;
    result = 31 * result + (userType != null ? userType.hashCode() : 0);
    result = 31 * result + (userStatus != null ? userStatus.hashCode() : 0);
    result = 31 * result + (firstName != null ? firstName.hashCode() : 0);
    result = 31 * result + (secondName != null ? secondName.hashCode() : 0);
    result = 31 * result + (login != null ? login.hashCode() : 0);
    result = 31 * result + (createdDate != null ? createdDate.hashCode() : 0);
    result = 31 * result + (closedDate != null ? closedDate.hashCode() : 0);
    return result;
  }

  @Override
  public int compareTo(User user) {
    return this.id > user.id ? 1 : this.id < user.id ? -1 : 0;
  }

  @Override
  public String toString() {
    return "User{"
            + "hashCode=" + hashCode()
            + ", id=" + id
            + ", userType=" + userType
            + ", userStatus=" + userStatus
            + ", firstName='" + firstName + '\''
            + ", secondName='" + secondName + '\''
            + ", login='" + login + '\''
            + ", createdDate=" + createdDate
            + ", closedDate=" + closedDate
            + '}';
  }

  // *** Enums *** //

  /**
   * User's types.
   */
  public enum UserType {
    ADMIN, USER
  }

  /**
   * User's statuses.
   */
  public enum UserStatus {
    ACTIVE, CLOSED
  }
}