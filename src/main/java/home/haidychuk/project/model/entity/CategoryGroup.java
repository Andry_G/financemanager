package home.haidychuk.project.model.entity;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

/**
 * Class for reflect table CatergoryGroup.
 * @author Andrii Haidychuk created 10.10.2017
 * @version 1.0
 */
@Entity
@Table(name = "Categorygroup")
public class CategoryGroup implements Serializable, Comparable<CategoryGroup> {
  // *** Fields *** //

  @Id
  @GeneratedValue
  @Column(name = "id", nullable = false)
  private Integer id;

  /**
   * Relationship to table User.
   */
  @ManyToOne(fetch = FetchType.EAGER)
  @JoinColumn(name = "user_id", nullable = false)
  private User user;

  @Size(min = 2, max = 35)
  @Pattern(regexp = "^[\\D\\S].+")
  @Column(name = "name", length = 35, nullable = false)
  private String name;

  /**
   * Relationship to table Category.
   */
  @OneToMany(mappedBy = "categoryGroup", fetch = FetchType.LAZY,
             cascade = {CascadeType.PERSIST, CascadeType.REFRESH,
                        CascadeType.MERGE, CascadeType.DETACH})
  private Collection<Category> categories;

  /**
   * Relationship to table Budget.
   */
  @ManyToMany(fetch = FetchType.LAZY)
  @JoinTable(name = "Budgetcategorygroup",
             joinColumns = @JoinColumn(name = "category_group_id", nullable = false),
             inverseJoinColumns = @JoinColumn(name = "budget_id", nullable = false)
  )
  private Collection<Budget> budgets;

  // *** Constructors *** //

  /**
   * Default constructor.
   */
  public CategoryGroup() {
  }

  /**
   * Constructor for creating new CategoryGroup.
   * @param user User who has this CategoryGroup
   * @param name Name for Category
   */
  public CategoryGroup(User user, String name) {
    this.user = user;
    this.name = name;
  }

  // *** Setters *** //

  public void setId(Integer id) {
    this.id = id;
  }

  public void setUser(User user) {
    this.user = user;
  }

  public void setName(String name) {
    this.name = name;
  }

  public void setCategories(Collection<Category> categories) {
    this.categories = categories;
  }

  public void setBudgets(Collection<Budget> budgets) {
    this.budgets = budgets;
  }

  // *** Getters *** //

  public Integer getId() {
    return id;
  }

  public User getUser() {
    return user;
  }

  public String getName() {
    return name;
  }

  public Collection<Category> getCategories() {
    return categories;
  }

  public Collection<Budget> getBudgets() {
    return budgets;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }

    CategoryGroup that = (CategoryGroup) o;

    if (id != null ? !id.equals(that.id) : that.id != null) {
      return false;
    }
    return name != null ? name.equals(that.name) : that.name == null;
  }

  @Override
  public int hashCode() {
    int result = id != null ? id.hashCode() : 0;
    result = 31 * result + (name != null ? name.hashCode() : 0);
    return result;
  }

  @Override
  public int compareTo(CategoryGroup categoryGroup) {
    return this.id > categoryGroup.id ? 1 : this.id < categoryGroup.id ? -1 : 0;
  }

  @Override
  public String toString() {
    return "CategoryGroup{"
            + "hascode=" + hashCode()
            + ", id=" + id
            + ", name='" + name + '\''
            + '}';
  }
}
