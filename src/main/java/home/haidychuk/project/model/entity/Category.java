package home.haidychuk.project.model.entity;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

/**
 * Class for reflect table Category.
 * @author Andrii Haidychuk created date 10.10.2017
 * @version 1.0
 */
@Entity
@Table(name = "Category")
public class Category implements Serializable, Comparable<Category> {
  // *** Fields *** //

  @Id
  @GeneratedValue
  @Column(name = "id", nullable = false)
  private Integer id;

  /**
   * Relationship to table CategoryGroup.
   */
  @ManyToOne(fetch = FetchType.EAGER)
  @JoinColumn(name = "group_id", nullable = false)
  private CategoryGroup categoryGroup;

  @Size(min = 2, max = 35)
  @Pattern(regexp = "^[\\D\\S].+")
  @Column(name = "name", length = 35, nullable = false)
  private String name;

  @Enumerated(EnumType.STRING)
  @Column(name = "category_type", length = 15, nullable = false)
  private CategoryType categoryType;

  /**
   * Relationship to table Budget.
   */
  @ManyToMany(fetch = FetchType.LAZY)
  @JoinTable(name = "Budgetcategory",
          joinColumns = @JoinColumn(name = "category_id", nullable = false),
          inverseJoinColumns = @JoinColumn(name = "budget_id", nullable = false)
  )
  private Collection<Budget> budgets;

  /**
   * Relationship to table Operation.
   */
  @OneToMany(mappedBy = "category", fetch = FetchType.LAZY,
             cascade = {CascadeType.PERSIST, CascadeType.REFRESH,
                        CascadeType.MERGE, CascadeType.DETACH})
  private Collection<Operation> operations;

  // *** Constructors *** //

  /**
   * Default constructor.
   */
  public Category() {
  }

  /**
   * Constructor for creating new Category.
   * @param categoryGroup CategoryGroup for Category
   * @param name Category name
   * @param categoryType type of the Category
   */
  public Category(CategoryGroup categoryGroup, String name, CategoryType categoryType) {
    this.categoryGroup = categoryGroup;
    this.name = name;
    this.categoryType = categoryType;
  }

  // *** Setters *** //

  public void setId(Integer id) {
    this.id = id;
  }

  public void setCategoryGroup(CategoryGroup categoryGroup) {
    this.categoryGroup = categoryGroup;
  }

  public void setName(String name) {
    this.name = name;
  }

  public void setCategoryType(CategoryType categoryType) {
    this.categoryType = categoryType;
  }

  public void setBudgets(Collection<Budget> budgets) {
    this.budgets = budgets;
  }

  public void setOperations(Collection<Operation> operations) {
    this.operations = operations;
  }

  // *** Getters *** //

  public Integer getId() {
    return id;
  }

  public CategoryGroup getCategoryGroup() {
    return categoryGroup;
  }

  public String getName() {
    return name;
  }

  public CategoryType getCategoryType() {
    return categoryType;
  }

  public Collection<Budget> getBudgets() {
    return budgets;
  }

  public Collection<Operation> getOperations() {
    return operations;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }

    Category category = (Category) o;

    if (id != null ? !id.equals(category.id) : category.id != null) {
      return false;
    }
    if (name != null ? !name.equals(category.name) : category.name != null) {
      return false;
    }
    return categoryType != null
            ? categoryType.equals(category.categoryType) : category.categoryType == null;
  }

  @Override
  public int hashCode() {
    int result = id != null ? id.hashCode() : 0;
    result = 31 * result + (name != null ? name.hashCode() : 0);
    result = 31 * result + (categoryType != null ? categoryType.hashCode() : 0);
    return result;
  }

  @Override
  public int compareTo(Category category) {
    return this.id > category.id ? 1 : this.id < category.id ? -1 : 0;
  }

  @Override
  public String toString() {
    return "Category{"
            + "hascode=" + hashCode()
            + ", id=" + id
            + ", name='" + name + '\''
            + ", categoryType=" + categoryType
            + '}';
  }

  // *** Enums *** //

  /**
   * Category types.
   */
  public enum CategoryType {
    INCOME, EXPENSE, UNIVERSAL
  }
}
