package home.haidychuk.project.model.entity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Collection;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.PrePersist;
import javax.persistence.Table;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

/**
 * Class for reflect table Account.
 * @author Andrii Haidychuk created on 12.07.2017
 * @version 1.0
 */
@Entity
@Table(name = "Account")
public class Account implements Serializable, Comparable<Account> {
  // *** Fields *** //

  @Id
  @GeneratedValue
  @Column(name = "id", nullable = false)
  private Integer id;

  /**
   * Relationship to the table User.
   */
  @ManyToOne(fetch = FetchType.EAGER)
  @JoinColumn(name = "user_id", nullable = false)
  private User user;

  /**
   * Relationship to the table Currency.
   */
  @ManyToOne(fetch = FetchType.EAGER)
  @JoinColumn(name = "currency_id", nullable = false)
  private Currency currency;

  @Size(min = 2, max = 35)
  @Pattern(regexp = "^[\\D\\S].+")
  @Column(name = "name", length = 35, nullable = false)
  private String name;

  @Column(name = "balance", nullable = false)
  private BigDecimal balance;

  @Column(name = "total_included", nullable = false)
  private Boolean totalIncluded;

  /**
   * Relationship to table Budget.
   */
  @ManyToMany(fetch = FetchType.LAZY)
  @JoinTable(name = "Budgetaccount",
             joinColumns = @JoinColumn(name = "account_id", nullable = false),
             inverseJoinColumns = @JoinColumn(name = "budget_id", nullable = false)
  )
  private Collection<Budget> budgets;

  /**
   * Relationship to table Operation as source.
   */
  @OneToMany(mappedBy = "source", fetch = FetchType.LAZY,
             cascade = {CascadeType.PERSIST, CascadeType.REFRESH,
                        CascadeType.MERGE, CascadeType.DETACH})
  private Collection<Operation> sourceOperations;

  /**
   * Relationship to table Operation as destination.
   */
  @OneToMany(mappedBy = "destination", fetch = FetchType.LAZY,
             cascade = {CascadeType.PERSIST, CascadeType.REFRESH,
                        CascadeType.MERGE, CascadeType.DETACH})
  private Collection<Operation> destinationOperations;

  /**
   * Set on default value for totalIncluded.
   */
  @PrePersist
  public void prePersistValues() {
    if (totalIncluded == null) {
      totalIncluded = true;
    }
  }

  // *** Constructors *** //

  /**
   * Default constructor.
   */
  public Account() {
  }

  /**
   * Constructor for creating new Account with default value for totalIncluded.
   * @param user User for Account
   * @param currency Currency for Account
   * @param name Account's name
   * @param balance Account's balance
   */
  public Account(User user, Currency currency, String name, BigDecimal balance) {
    this.user = user;
    this.currency = currency;
    this.name = name;
    this.balance = balance;
  }

  /**
   * Constructor for creating actual Account or new Account with specified
   * value for totalIncluded.
   * @param user User for Account
   * @param currency Currency for Account
   * @param name Account's name
   * @param balance Account's balance
   * @param totalIncluded Flag that shows whether this Account will be included in total amount
   *                       for appropriate User.
   */
  public Account(User user, Currency currency, String name, BigDecimal balance,
                 Boolean totalIncluded) {
    this.user = user;
    this.currency = currency;
    this.name = name;
    this.balance = balance;
    this.totalIncluded = totalIncluded;
  }

  // *** Setters *** //

  public void setId(Integer id) {
    this.id = id;
  }

  public void setUser(User user) {
    this.user = user;
  }

  public void setCurrency(Currency currency) {
    this.currency = currency;
  }

  public void setName(String name) {
    this.name = name;
  }

  public void setBalance(BigDecimal balance) {
    this.balance = balance;
  }

  public void setTotalIncluded(Boolean totalIncluded) {
    totalIncluded = totalIncluded;
  }

  public void setBudgets(Collection<Budget> budgets) {
    this.budgets = budgets;
  }

  public void setSourceOperations(Collection<Operation> sourceOperations) {
    this.sourceOperations = sourceOperations;
  }

  public void setDestinationOperations(Collection<Operation> destinationOperations) {
    this.destinationOperations = destinationOperations;
  }

  // *** Getters *** //

  public Integer getId() {
    return id;
  }

  public User getUser() {
    return user;
  }

  public Currency getCurrency() {
    return currency;
  }

  public String getName() {
    return name;
  }

  public BigDecimal getBalance() {
    return balance;
  }

  public Boolean getTotalIncluded() {
    return totalIncluded;
  }

  public Collection<Budget> getBudgets() {
    return budgets;
  }

  public Collection<Operation> getSourceOperations() {
    return sourceOperations;
  }

  public Collection<Operation> getDestinationOperations() {
    return destinationOperations;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }

    Account account = (Account) o;

    if (id != null ? !id.equals(account.id) : account.id != null) {
      return false;
    }
    if (name != null ? !name.equals(account.name) : account.name != null) {
      return false;
    }
    if (balance != null ? !balance.equals(account.balance) : account.balance != null) {
      return false;
    }
    return totalIncluded != null
            ? totalIncluded.equals(account.totalIncluded) : account.totalIncluded == null;
  }

  @Override
  public int hashCode() {
    int result = id != null ? id.hashCode() : 0;
    result = 31 * result + (name != null ? name.hashCode() : 0);
    result = 31 * result + (balance != null ? balance.hashCode() : 0);
    result = 31 * result + (totalIncluded != null ? totalIncluded.hashCode() : 0);
    return result;
  }

  @Override
  public int compareTo(Account account) {
    return this.id > account.id ? 1 : this.id < account.id ? -1 : 0;
  }

  @Override
  public String toString() {
    return "Account{"
            + "hashCode=" + hashCode()
            + ", id=" + id
            + ", name='" + name + '\''
            + ", balance=" + balance
            + ", totalIncluded=" + totalIncluded
            + '}';
  }
}
