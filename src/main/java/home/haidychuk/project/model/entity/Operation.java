package home.haidychuk.project.model.entity;

import java.io.Serializable;

import java.math.BigDecimal;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.PrePersist;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.Size;

/**
 * Class for reflect table Operation.
 * @author Andrii Haidychuk created date 22.11.2017
 * @version 1.0
 */
@Entity
@Table(name = "Operation")
public class Operation implements Serializable, Comparable<Operation> {
  // *** Fields *** //

  @Id
  @GeneratedValue
  @Column(name = "id", nullable = false)
  private Integer id;

  /**
   * Relationship to table Category.
   */
  @ManyToOne(fetch = FetchType.EAGER)
  @JoinColumn(name = "category_id", nullable = false)
  private Category category;

  /**
   * Relationship to table Account as "source".
   */
  @ManyToOne(fetch = FetchType.EAGER)
  @JoinColumn(name = "source_id")
  private Account source;

  /**
   * Relationship to table Account as "destination".
   */
  @ManyToOne(fetch = FetchType.EAGER)
  @JoinColumn(name = "destination_id")
  private Account destination;

  @Enumerated(EnumType.ORDINAL)
  @Column(name = "operation_type", length = 15, nullable = false)
  private OperationType operationType;

  @Temporal(TemporalType.DATE)
  @Column(name = "date", nullable = false)
  private Date date;

  @Column(name = "amount", nullable = false)
  private BigDecimal amount;

  @Column(name = "rate", nullable = false)
  private BigDecimal rate;

  @Size(max = 100)
  @Column(name = "comment", length = 100)
  private String comment;

  /**
   * Set on default values for Operation.
   */
  @PrePersist
  public void prePersistValues() {
    if (date == null) {
      date = new Date();
    }

    if (operationType == null) {
      operationType = OperationType.EXPENSE;
    }

    if (rate == null) {
      rate = BigDecimal.ONE;
    }
  }

  // *** Constructors *** //

  /**
   * Defauld constructor.
   */
  public Operation() {
  }

  /**
   * Constructor for creating new Operation.
   * @param category operation category
   * @param source operation source account
   * @param destination operation destination account
   * @param operationType OperationType value
   * @param date operation date
   * @param amount operation amount
   * @param rate operation rate
   */
  public Operation(Category category, Account source, Account destination,
                   OperationType operationType, Date date, BigDecimal amount, BigDecimal rate) {
    this.category = category;
    this.source = source;
    this.destination = destination;
    this.operationType = operationType;
    this.date = date;
    this.amount = amount;
    this.rate = rate;
  }

  // *** Setters *** //

  public void setId(Integer id) {
    this.id = id;
  }

  public void setCategory(Category category) {
    this.category = category;
  }

  public void setSource(Account source) {
    this.source = source;
  }

  public void setDestination(Account destination) {
    this.destination = destination;
  }

  public void setOperationType(OperationType operationType) {
    this.operationType = operationType;
  }

  public void setDate(Date date) {
    this.date = date;
  }

  public void setAmount(BigDecimal amount) {
    this.amount = amount;
  }

  public void setRate(BigDecimal rate) {
    this.rate = rate;
  }

  public void setComment(String comment) {
    this.comment = comment;
  }

  // *** Getters *** //

  public Integer getId() {
    return id;
  }

  public Category getCategory() {
    return category;
  }

  public Account getSource() {
    return source;
  }

  public Account getDestination() {
    return destination;
  }

  public OperationType getOperationType() {
    return operationType;
  }

  public Date getDate() {
    return date;
  }

  public BigDecimal getAmount() {
    return amount;
  }

  public BigDecimal getRate() {
    return rate;
  }

  public String getComment() {
    return comment;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }

    Operation operation = (Operation) o;

    if (id != null ? !id.equals(operation.id) : operation.id != null) {
      return false;
    }
    if (operationType != operation.operationType) {
      return false;
    }
    if (date != null ? !date.equals(operation.date) : operation.date != null) {
      return false;
    }
    if (amount != null ? !amount.equals(operation.amount) : operation.amount != null) {
      return false;
    }
    if (rate != null ? !rate.equals(operation.rate) : operation.rate != null) {
      return false;
    }
    return comment != null ? comment.equals(operation.comment) : operation.comment == null;
  }

  @Override
  public int hashCode() {
    int result = id != null ? id.hashCode() : 0;
    result = 31 * result + (operationType != null ? operationType.hashCode() : 0);
    result = 31 * result + (date != null ? date.hashCode() : 0);
    result = 31 * result + (amount != null ? amount.hashCode() : 0);
    result = 31 * result + (rate != null ? rate.hashCode() : 0);
    result = 31 * result + (comment != null ? comment.hashCode() : 0);
    return result;
  }

  @Override
  public int compareTo(Operation operation) {
    return this.id > operation.id ? 1 : this.id < operation.id ? -1 : 0;
  }

  @Override
  public String toString() {
    return "Operation{"
            + "id=" + id
            + ", source=" + source != null ? source.getName() : ""
            + ", destination=" + destination != null ? destination.getName() : ""
            + ", operationType=" + operationType
            + ", date=" + date
            + ", amount=" + amount
            + ", rate=" + rate
            + ", comment='" + comment + '\''
            + '}';
  }

  // *** Enums *** //

  /**
   * Operation types.
   */
  public enum OperationType {
    EXPENSE, INCOME, TRANSFER;
  }
}
