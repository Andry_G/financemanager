package home.haidychuk.project.model.exception.exceptions;

/**
 * Exception class if User status can't be changed.
 * @author Andrii Haidychuk created on 08.10.2018
 * @version 1.0
 */
public class RejectChangeUserStatusException extends RuntimeException {
  // *** Constructor ***//

  /**
   * Create exception's object.
   * @param message exception message
   */
  public RejectChangeUserStatusException(String message) {
    super(message);
  }
}
