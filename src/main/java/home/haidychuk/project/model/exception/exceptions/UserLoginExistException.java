package home.haidychuk.project.model.exception.exceptions;

/**
 * Exception class if specify login already exist.
 * @author Andrii Haidychuk created on 02.10.2018
 * @version 1.0
 */
public class UserLoginExistException extends RuntimeException {
  // *** Constructor ***//

  /**
   * Create exception's object.
   * @param message exception message
   */
  public UserLoginExistException(String message) {
    super(message);
  }
}
