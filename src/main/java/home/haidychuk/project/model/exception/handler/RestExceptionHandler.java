package home.haidychuk.project.model.exception.handler;

import home.haidychuk.project.model.exception.exceptions.NoSuchUserStatusException;
import home.haidychuk.project.model.exception.exceptions.NoSuchUserTypeException;
import home.haidychuk.project.model.exception.exceptions.RejectChangeUserStatusException;
import home.haidychuk.project.model.exception.exceptions.UserLoginExistException;
import home.haidychuk.project.model.exception.exceptions.UserNotFoundException;
import home.haidychuk.project.model.exception.structure.ApiErrorDetails;

import java.util.Arrays;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.HttpRequestMethodNotSupportedException;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.NoHandlerFoundException;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

/**
 * General API exception handler class.
 * @author Andrii Haidychuk created on 27.09.2018
 * @version 1.0
 */
@ControllerAdvice
public class RestExceptionHandler extends ResponseEntityExceptionHandler {
  // *** Fields *** //

  private HttpHeaders httpHeaders = new HttpHeaders();

  //fill HttpHeaders
  {
    httpHeaders.put(HttpHeaders.CONTENT_TYPE,
            Arrays.asList(MediaType.APPLICATION_JSON_UTF8.toString()));
  }

  /**
   * Requested entity doesn't exist.
   * @param ex exception's type
   * @param request Http request
   * @return ResponseEntity object
   */
  @ExceptionHandler(UserNotFoundException.class)
  public ResponseEntity<ApiErrorDetails> handlerEntityNotFound(
          RuntimeException ex, WebRequest request) {

    ApiErrorDetails errorDetails = new ApiErrorDetails(HttpStatus.NOT_FOUND, ex, request);

    return new ResponseEntity<ApiErrorDetails>(errorDetails, httpHeaders, HttpStatus.NOT_FOUND);
  }

  /**
   * Specified User's login already exist.
   * @param ex exception's type
   * @param request Http request
   * @return ResponseEntity object
   */
  @ExceptionHandler({UserLoginExistException.class, RejectChangeUserStatusException.class})
  public ResponseEntity<ApiErrorDetails> handlerUserLoginExist(
          RuntimeException ex, WebRequest request) {

    ApiErrorDetails errorDetails = new ApiErrorDetails(HttpStatus.NOT_ACCEPTABLE, ex, request);

    return new ResponseEntity<ApiErrorDetails>(
            errorDetails, httpHeaders, HttpStatus.NOT_ACCEPTABLE);
  }

  /**
   * Specified User type is not valid.
   * @param ex exception's type
   * @param request Http request
   * @return ResponseEntity object
   */
  @ExceptionHandler({NoSuchUserTypeException.class, NoSuchUserStatusException.class})
  public ResponseEntity<ApiErrorDetails> handlerUserTypeInvalid(
          RuntimeException ex, WebRequest request) {

    ApiErrorDetails errorDetails = new ApiErrorDetails(HttpStatus.BAD_REQUEST, ex, request);

    return new ResponseEntity<ApiErrorDetails>(
            errorDetails, httpHeaders, HttpStatus.BAD_REQUEST);
  }

  @Override
  protected ResponseEntity<Object> handleMethodArgumentNotValid(
          MethodArgumentNotValidException ex, HttpHeaders headers,
          HttpStatus status, WebRequest request) {

    ApiErrorDetails errorDetails = new ApiErrorDetails(status, ex, request);

    return handleExceptionInternal(ex, errorDetails, httpHeaders, HttpStatus.BAD_REQUEST, request);
  }

  @Override
  protected ResponseEntity<Object> handleNoHandlerFoundException(
          NoHandlerFoundException ex, HttpHeaders headers, HttpStatus status, WebRequest request) {

    ApiErrorDetails errorDetails = new ApiErrorDetails(HttpStatus.NOT_IMPLEMENTED, ex, request);

    return handleExceptionInternal(
            ex, errorDetails, httpHeaders, HttpStatus.NOT_IMPLEMENTED, request);
  }

  @Override
  protected ResponseEntity<Object> handleHttpRequestMethodNotSupported(
          HttpRequestMethodNotSupportedException ex, HttpHeaders headers,
          HttpStatus status, WebRequest request) {

    StringBuilder builder = new StringBuilder();
    builder.append(ex.getMethod());
    builder.append(" method is not supported for this request. Supported methods are ");
    ex.getSupportedHttpMethods().forEach(a -> builder.append(a + ", "));

    ApiErrorDetails errorDetails = new ApiErrorDetails(HttpStatus.METHOD_NOT_ALLOWED, ex, request);
    errorDetails.setMessage(builder.toString());

    return handleExceptionInternal(
            ex, errorDetails, httpHeaders, HttpStatus.METHOD_NOT_ALLOWED, request);
  }
}
