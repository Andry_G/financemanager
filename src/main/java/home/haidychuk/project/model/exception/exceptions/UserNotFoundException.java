package home.haidychuk.project.model.exception.exceptions;

/**
 * Exception class if User is not found.
 * @author Andrii Haidychuk created on 27.09.2018
 * @version 1.0
 */
public class UserNotFoundException extends RuntimeException {
  // *** Constructor ***//

  /**
   * Create exception's object.
   * @param message exception message
   */
  public UserNotFoundException(String message) {
    super(message);
  }
}
