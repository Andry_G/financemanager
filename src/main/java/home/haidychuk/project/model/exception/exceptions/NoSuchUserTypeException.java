package home.haidychuk.project.model.exception.exceptions;

/**
 * Exception class if User type is not correct.
 * @author Andrii Haidychuk created on 08.10.2018
 * @version 1.0
 */
public class NoSuchUserTypeException extends RuntimeException {
  // *** Constructor ***//

  /**
   * Create exception's object.
   * @param message exception message
   */
  public NoSuchUserTypeException(String message) {
    super(message);
  }
}
