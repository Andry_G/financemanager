package home.haidychuk.project.model.exception.structure;

/**
 * Class contain detail information regarding validation error by fields.
 * @author Andrii Haidychuk created on 27.09.2018
 * @version 1.0
 */
public class ApiSubErrors {
  // *** Fields *** //

  private String object;
  private String field;
  private Object rejectedValue;
  private String message;

  // *** Constructors *** //

  /**
   * Creating new object ApiSubErrors.
   * @param object Object with error
   * @param field field of incorrect object
   * @param rejectedValue invalid value
   * @param message error description
   */
  public ApiSubErrors(String object, String field, Object rejectedValue,  String message) {
    this.object = object;
    this.field = field;
    this.rejectedValue = rejectedValue;
    this.message = message;
  }

  // *** Getters *** //

  public String getObject() {
    return object;
  }

  public String getField() {
    return field;
  }

  public Object getRejectedValue() {
    return rejectedValue;
  }

  public String getMessage() {
    return message;
  }
}
