package home.haidychuk.project.model.exception.exceptions;

/**
 * Exception class if User status is not correct.
 * @author Andrii Haidychuk created on 08.10.2018
 * @version 1.0
 */
public class NoSuchUserStatusException extends RuntimeException {
  // *** Constructor ***//

  /**
   * Create exception's object.
   * @param message exception message
   */
  public NoSuchUserStatusException(String message) {
    super(message);
  }
}
