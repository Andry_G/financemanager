package home.haidychuk.project.model.exception.structure;

import com.fasterxml.jackson.annotation.JsonFormat;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.http.HttpStatus;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.context.request.WebRequest;

/**
 * Class contain detail information regarding API error.
 * @author Andrii Haidychuk created on 27.09.2018
 * @version 1.0
 */
public class ApiErrorDetails {
  // *** Fields *** //

  @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy HH:mm:ss")
  private Date timestamp;
  private int httpStatusCode;
  private HttpStatus httpStatusReason;
  private String message;
  private String exceptionClass;
  private List<ApiSubErrors> subErrors;
  private String path;

  // *** Constructors *** //

  /**
   * Constructor for creating new ApiErrorDetail without list of subErrors.
   * @param httpStatusReason Error's http status
   * @param exception Exception's object
   * @param request WebRequest object
   */
  public ApiErrorDetails(HttpStatus httpStatusReason, Throwable exception, WebRequest request) {
    this.timestamp = new Date();
    this.httpStatusCode = httpStatusReason.value();
    this.httpStatusReason = httpStatusReason;
    this.message = exception.getMessage();
    this.exceptionClass = exception.getClass().getName();
    this.subErrors = new ArrayList<>();
    this.path = request.getDescription(false);
    // init list subErrors
    if (exception instanceof MethodArgumentNotValidException) {
      MethodArgumentNotValidException validException = (MethodArgumentNotValidException) exception;

      for (FieldError error : validException.getBindingResult().getFieldErrors()) {
        ApiSubErrors apiSubErrors = new ApiSubErrors(
                error.getObjectName(), error.getField(),
                error.getRejectedValue(), error.getDefaultMessage()
        );
        subErrors.add(apiSubErrors);
      }
    }
  }

  // *** Getters *** //

  public Date getTimestamp() {
    return timestamp;
  }

  public int getHttpStatusCode() {
    return httpStatusCode;
  }

  public HttpStatus getHttpStatusReason() {
    return httpStatusReason;
  }

  public String getMessage() {
    return message;
  }

  public String getExceptionClass() {
    return exceptionClass;
  }

  public List<ApiSubErrors> getSubErrors() {
    return new ArrayList<ApiSubErrors>(subErrors);
  }

  public String getPath() {
    return path;
  }

  //*** Setters ***//

  public void setMessage(String message) {
    this.message = message;
  }
}
