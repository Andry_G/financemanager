package home.haidychuk.project.model.service;

import home.haidychuk.project.model.entity.User;

import java.util.Collection;
import java.util.Date;

/**
 * Interface for working with User in service layout.
 * @author Andrii Haidychuk created on 02.04.2018
 * @version 1.0
 */
public interface UserService {

  /**
   * Find User be user id.
   * @param id user id
   * @return User for specified id
   */
  User findById(Integer id);

  /**
   * Find all Users.
   * @return Collection of all Users
   */
  Collection<User> findAll();

  /**
   * Create new User.
   * @param user new User
   * @return created User
   */
  User create(User user);

  /**
   * Update existed User.
   * @param user User for updating
   * @return updated User
   */
  User update(User user);

  /**
   * Delete existed User if he doesn't have any child entity, otherwise changes
   * his status on CLOSED.
   * @param id User id for deleting
   * @return deleted User
   */
  User delete(Integer id);

  /**
   * Activating User, namely change his status form CLOSED to ACTIVE if such user
   * hasn't been closed before and also set closedDate as current date.
   * @param id User's id for activating
   * @return activated User or null if such user is already in CLOSED status.
   */
  User activateUser(Integer id);

  /**
   * DeActivating User, namely change his status form ACTIVE to CLOSED if such user
   * has been closed before and also set closedDate.
   * @param id User's id for deactivating
   * @return deactivated User or null if such user is already in CLOSED status.
   */
  User deactivateUser(Integer id);

  /**
   * Find User by user type.
   * @param userType User's type in string representation
   * @return Collection of User object
   */
  Collection<User> findByUserType(String userType);

  /**
   * Find User by user status.
   * @param userStatus User's status
   * @return Collection of User object
   */
  Collection<User> findByUserStatus(String userStatus);

  /**
   * Find User by user first name.
   * @param firstName User's first name
   * @return Collection of User object
   */
  Collection<User> findByFirstName(String firstName);

  /**
   * Find User by user second name.
   * @param secondName User's second name
   * @return Collection of User object
   */
  Collection<User> findBySecondName(String secondName);

  /**
   * Find User by user created date.
   * @param createdDate User's created date
   * @return Collection of User object
   */
  Collection<User> findByCreatedDate(Date createdDate);

  /**
   * Find User by user created date.
   * @param closedDate User's created date
   * @return Collection of User object
   */
  Collection<User> findByClosedDate(Date closedDate);
}