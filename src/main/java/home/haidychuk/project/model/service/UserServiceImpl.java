package home.haidychuk.project.model.service;

import home.haidychuk.project.model.entity.User;
import home.haidychuk.project.model.exception.exceptions.NoSuchUserStatusException;
import home.haidychuk.project.model.exception.exceptions.NoSuchUserTypeException;
import home.haidychuk.project.model.exception.exceptions.RejectChangeUserStatusException;
import home.haidychuk.project.model.exception.exceptions.UserLoginExistException;
import home.haidychuk.project.model.exception.exceptions.UserNotFoundException;
import home.haidychuk.project.model.repository.UserRepository;

import java.text.SimpleDateFormat;
import java.util.Collection;
import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Service implementation for User.
 * @author Andrii Haidychuk created on 02.04.2018
 * @version 1.0
 */
@Service
public class UserServiceImpl implements UserService {

  @Autowired
  private UserRepository userRepository;

  @Override
  public User findById(Integer id) {
    User foundUser = userRepository.findOne(id);

    if (foundUser == null) {
      throw new UserNotFoundException("User with id '" + id + "' does not exist.");
    }

    return foundUser;
  }

  @Override
  public Collection<User> findAll() {
    Collection<User> foundUsers = userRepository.findAll();

    if (foundUsers.isEmpty()) {
      throw new UserNotFoundException("There is no any user in system for the time being.");
    }

    return foundUsers;
  }

  @Override
  public User create(User user) {
    String login = user.getLogin();

    if (userRepository.findByLogin(login) != null) {
      throw new UserLoginExistException(
              "Login '" + login + "' already exists, you can not use it.");
    }

    User newUser = new User(user.getUserType(), user.getUserStatus(), user.getFirstName(),
            user.getSecondName(), user.getLogin(), user.getPassword());

    return userRepository.save(newUser);
  }
  //TODO Authorized user must have ADMIN role or be himself. Implement this after security level.

  @Override
  public User update(User user) {
    //Search origin User
    User originalUser = findById(user.getId());

    //Updating User
    originalUser.setFirstName(user.getFirstName());
    originalUser.setSecondName(user.getSecondName());
    originalUser.setLogin(user.getLogin());

    return userRepository.save(originalUser);
  }

  @Override
  public User delete(Integer id) {
    User foundUser = findById(id);

    if (foundUser.getCategoryGroups().isEmpty()
        && foundUser.getAccounts().isEmpty()
        && foundUser.getBudgets().isEmpty()) {

      userRepository.delete(foundUser.getId());
      return foundUser;
    }

    return deactivateUser(id);
  }

  //TODO Authorized user must have ADMIN role. Need to implement this after security level.
  @Override
  public User activateUser(Integer id) {
    User foundUser = findById(id);

    if (foundUser.getUserStatus() == User.UserStatus.ACTIVE) {
      throw new RejectChangeUserStatusException("This user has already had a active status.");
    }

    foundUser.setClosedDate(null);
    foundUser.setUserStatus(User.UserStatus.ACTIVE);
    userRepository.save(foundUser);

    return foundUser;
  }

  //TODO Authorized user must have ADMIN role. Need to implement this after security level.
  @Override
  public User deactivateUser(Integer id) {
    User foundUser = findById(id);

    if (foundUser.getUserStatus() == User.UserStatus.CLOSED) {
      throw new RejectChangeUserStatusException("This user has already had a closed status.");
    }

    foundUser.setClosedDate(new Date());
    foundUser.setUserStatus(User.UserStatus.CLOSED);
    userRepository.save(foundUser);

    return foundUser;
  }

  @Override
  public Collection<User> findByUserType(String requestedType) {
    User.UserType userType;

    try {
      userType = User.UserType.valueOf(requestedType.toUpperCase());

    } catch (IllegalArgumentException e) {
      StringBuilder builder = new StringBuilder();
      builder.append("User type '" + requestedType + "' is not correct. ");
      builder.append("Existed User types are");

      for (User.UserType type : User.UserType.values()) {
        builder.append(" " + type);
      }
      throw new NoSuchUserTypeException(builder.toString());
    }

    Collection<User> users = userRepository.findByUserType(userType);

    if (users.isEmpty()) {
      throw new UserNotFoundException("Users with type '" + userType + "' do not exist.");
    }

    return users;
  }

  @Override
  public Collection<User> findByUserStatus(String requestedStatus) {
    User.UserStatus userStatus;

    try {
      userStatus = User.UserStatus.valueOf(requestedStatus.toUpperCase());

    } catch (IllegalArgumentException e) {
      StringBuilder builder = new StringBuilder();
      builder.append("User status '" + requestedStatus + "' is not correct. ");
      builder.append("Existed User status are");

      for (User.UserStatus type : User.UserStatus.values()) {
        builder.append(" " + type);
      }
      throw new NoSuchUserStatusException(builder.toString());
    }

    Collection<User> users = userRepository.findByUserStatus(userStatus);

    if (users.isEmpty()) {
      throw new UserNotFoundException("Users with status '" + userStatus + "' do not exist");
    }

    return users;
  }

  @Override
  public Collection<User> findByFirstName(String firstName) {
    Collection<User> users = userRepository.findByFirstName(firstName);

    if (users.isEmpty()) {
      throw new UserNotFoundException("Users with first name '" + firstName + "' do not exist");
    }

    return users;
  }

  @Override
  public Collection<User> findBySecondName(String secondName) {
    Collection<User> users = userRepository.findBySecondName(secondName);

    if (users.isEmpty()) {
      throw new UserNotFoundException("Users with second name '" + secondName + "' do not exist");
    }

    return users;
  }

  @Override
  public Collection<User> findByCreatedDate(Date createdDate) {
    Collection<User> users = userRepository.findByCreatedDate(createdDate);
    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");

    if (users.isEmpty()) {
      throw new UserNotFoundException(
              "Users with created date '" + sdf.format(createdDate) + "' do not exist");
    }

    return users;
  }

  @Override
  public Collection<User> findByClosedDate(Date closedDate) {
    Collection<User> users = userRepository.findByClosedDate(closedDate);
    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");

    if (users.isEmpty()) {
      throw new UserNotFoundException(
              "Users with closed date '" + sdf.format(closedDate) + "' do not exist");
    }

    return users;
  }
}