package home.haidychuk.project.model.util;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 * Class for calculating date with requested day.
 * @author Andrii Haidychuk created on 02.03.2018
 * @version 1.0
 */
public class DateUtil {

  /**
   * Method retrieves date with first or last day for specified period in format "yyyy-MM-dd".
   * @param day Day instance related to needed day of month
   * @return Date with needed day of month
   */
  public static Date dateWithRequestedDay(Day day) {
    Calendar calendar = Calendar.getInstance();
    DateFormat df = new SimpleDateFormat("yyyy-MM-dd");

    switch (day) {
      //calculation the start day of the current week (Monday, value = 2)
      case FIRST_DAY_OF_WEEK:
        calendar.set(Calendar.DAY_OF_WEEK, 2);
        break;

      //calculation the last day of the current week
      case LAST_DAY_OF_WEEK:
        calendar.set(Calendar.DAY_OF_WEEK, 1);
        break;

      // calculation the start day of the last week (Monday, value = 2)
      case FIRST_DAY_OF_LAST_WEEK:
        calendar.add(Calendar.WEEK_OF_MONTH, -1);
        calendar.set(Calendar.DAY_OF_WEEK, 2);
        break;

      //calculation the last day of the last week
      case LAST_DAY_OF_LAST_WEEK:
        calendar.add(Calendar.WEEK_OF_MONTH, -1);
        calendar.set(Calendar.DAY_OF_WEEK, 1);
        break;

      //calculation the start day of the current month
      case FIRST_DAY_OF_MONTH:
        calendar.set(Calendar.DAY_OF_MONTH, 1);
        break;

      // calculation the last day of the current month
      case LAST_DAY_OF_MONTH:
        calendar.set(Calendar.DAY_OF_MONTH, calendar.getActualMaximum(Calendar.DAY_OF_MONTH));
        break;

      //calculation the start day of the last month
      case FIRST_DAY_OF_LAST_MONTH:
        calendar.add(Calendar.MONTH, -1);
        calendar.set(Calendar.DAY_OF_MONTH, 1);
        break;

      //calculation the last day of the last month
      case LAST_DAY_OF_LAST_MONTH:
        calendar.add(Calendar.MONTH, -1);
        calendar.set(Calendar.DAY_OF_MONTH, calendar.getActualMaximum(Calendar.DAY_OF_MONTH));
        break;

      //calculation the start day of the current year
      case FIRST_DAY_OF_YEAR:
        calendar.set(Calendar.DAY_OF_YEAR, 1);
        break;

      //calculation the last day of the current year
      case LAST_DAY_OF_YEAR:
        calendar.set(Calendar.DAY_OF_YEAR, calendar.getActualMaximum(Calendar.DAY_OF_YEAR));
        break;

      //calculation the start day of the last year
      case FIRST_DAY_OF_LAST_YEAR:
        calendar.add(Calendar.YEAR, -1);
        calendar.set(Calendar.DAY_OF_YEAR, 1);
        break;

      //calculation the last day of the last year
      case LAST_DAY_OF_LAST_YEAR:
        calendar.add(Calendar.YEAR, -1);
        calendar.set(Calendar.DAY_OF_YEAR, calendar.getActualMaximum(Calendar.DAY_OF_YEAR));
        break;

      default:
    }

    Date resultedDate = null;
    try {
      resultedDate = df.parse(df.format(calendar.getTime()));
    } catch (ParseException e) {
      e.printStackTrace();
    }

    return resultedDate;
  }

  public enum Day {
    FIRST_DAY_OF_WEEK, LAST_DAY_OF_WEEK,
    FIRST_DAY_OF_LAST_WEEK, LAST_DAY_OF_LAST_WEEK,
    FIRST_DAY_OF_MONTH, LAST_DAY_OF_MONTH,
    FIRST_DAY_OF_LAST_MONTH, LAST_DAY_OF_LAST_MONTH,
    FIRST_DAY_OF_YEAR, LAST_DAY_OF_YEAR,
    FIRST_DAY_OF_LAST_YEAR, LAST_DAY_OF_LAST_YEAR,
  }
}
