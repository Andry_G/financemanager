package home.haidychuk.project.model.repository;

import home.haidychuk.project.model.entity.Account;

import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Interface for working with Account in persistence layout.
 * @author Andrii Haidychuk created on 16.07.2017
 * @version 1.0
 */
public interface AccountRepository extends JpaRepository<Account, Integer> {

}
