package home.haidychuk.project.model.repository;

import home.haidychuk.project.model.entity.CategoryGroup;

import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Interface for working with CategoryGroup in persistence layout.
 * @author Andrii Haidychuk created on 27.11.2017
 * @version 1.0
 */
public interface CategoryGroupRepository extends JpaRepository<CategoryGroup, Integer> {

  /**
   * Find group of category by name.
   * @param name name of group of category
   * @return group of category
   */
  CategoryGroup findByName(String name);
}
