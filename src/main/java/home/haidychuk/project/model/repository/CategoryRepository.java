package home.haidychuk.project.model.repository;

import home.haidychuk.project.model.entity.Category;

import org.springframework.data.jpa.repository.JpaRepository;


/**
 * Interface for working with Category in persistence layout.
 * @author Andrii Haidychuk created on 27.11.2017
 * @version 1.0
 */
public interface CategoryRepository extends JpaRepository<Category, Integer> {

  /**
   * Find category by it's name.
   * @param name name of Category
   * @return Category
   */
  Category findByName(String name);
}
