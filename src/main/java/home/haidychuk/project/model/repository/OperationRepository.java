package home.haidychuk.project.model.repository;

import home.haidychuk.project.model.entity.Operation;

import java.util.Collection;

import org.springframework.data.jpa.repository.JpaRepository;


/**
 * Interface for working with Operation in persistence layout.
 * @author Andrii Haidychuk created on 29.11.2017
 * @version 1.0
 */
public interface OperationRepository extends JpaRepository<Operation, Integer> {

  /**
   * Find operations by specified operation type.
   * @param operationType type of operation
   * @return collection of operation
   */
  Collection<Operation> findByOperationType(Operation.OperationType operationType);
}
