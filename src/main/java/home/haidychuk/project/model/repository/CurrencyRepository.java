package home.haidychuk.project.model.repository;

import home.haidychuk.project.model.entity.Currency;

import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Interface for working with Currency in persistence layout.
 * @author Andrii Haidychuk created on 16.07.2017
 * @version 1.0
 */
public interface CurrencyRepository extends JpaRepository<Currency, Integer> {

  /**
   * Find Currency by short title.
   * @param shortTitle Currency's short title.
   * @return Currency object
   */
  Currency findByShortTitle(String shortTitle);

  /**
   * Find Currency by title.
   * @param title Currency's title
   * @return Currency object
   */
  Currency findByTitle(String title);
}
