package home.haidychuk.project.model.repository;

import home.haidychuk.project.model.entity.User;

import java.util.Collection;
import java.util.Date;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

/**
 * Interface for working with User in persistence layout.
 * @author Andrii Haidychuk created on 16.07.2017
 * @version 1.0
 */
public interface UserRepository extends JpaRepository<User, Integer> {

  /**
   * Find User by user type.
   * @param userType User's type
   * @return Collection of User object
   */
  Collection<User> findByUserType(User.UserType userType);

  /**
   * Find User by user status.
   * @param userStatus User's status
   * @return Collection of User object
   */
  Collection<User> findByUserStatus(User.UserStatus userStatus);

  /**
   * Find User by user first name.
   * @param firstName User's first name
   * @return Collection of User object
   */
  Collection<User> findByFirstName(String firstName);

  /**
   * Find User by user second name.
   * @param secondName User's second name
   * @return Collection of User object
   */
  Collection<User> findBySecondName(String secondName);

  /**
   * Find User by user login.
   * @param login User's login
   * @return User object
   */
  User findByLogin(String login);

  /**
   * Find User by user created date.
   * @param createdDate User's created date
   * @return Collection of User object
   */
  Collection<User> findByCreatedDate(Date createdDate);

  /**
   * Find User by user created date.
   * @param closedDate User's created date
   * @return Collection of User object
   */
  Collection<User> findByClosedDate(Date closedDate);
}