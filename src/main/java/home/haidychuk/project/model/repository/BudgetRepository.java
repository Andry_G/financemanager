package home.haidychuk.project.model.repository;

import home.haidychuk.project.model.entity.Account;
import home.haidychuk.project.model.entity.Budget;
import home.haidychuk.project.model.entity.Category;
import home.haidychuk.project.model.entity.CategoryGroup;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

/**
 * Interface for working with Budget in persistence layout.
 * @author Andrii Haidychuk created on 07.12.2017
 * @version 1.0
 */
public interface BudgetRepository extends JpaRepository<Budget, Integer> {

  /**
   * Add specified Account to current Budget.
   * @param budget Budget to which Account will be added
   * @param account Account that have to be added to specified Budget
   */
  @Modifying
  @Query(value = "INSERT INTO Budgetaccount (budget_id, account_id) VALUES (:budget, :account)",
         nativeQuery = true)
  void addAccountToBudget(
        @Param("budget") Budget budget,
        @Param("account") Account account
  );

  /**
   * Delete specified Account from current Budget.
   * @param budget Budget from which Account will be removed
   * @param account Account that have to be removed from specified Budget
   */
  @Modifying
  @Query(value = "DELETE FROM Budgetaccount WHERE budget_id = :budget AND account_id = :account",
         nativeQuery = true)
  void removeAccountFromBudget(
          @Param("budget") Budget budget,
          @Param("account") Account account
  );

  /**
   * Add CategoryGroup to specified Budget.
   * @param budget Budget to which CategoryGroup will be added
   * @param categoryGroup CategoryGroup that have to be added to specified Budget
   */
  @Modifying
  @Query(value = "INSERT INTO Budgetcategorygroup (budget_id, category_group_id) "
               + "VALUES (:budget, :categorygroup)",
         nativeQuery = true)
  void addCategoryGroupToBudget(
          @Param("budget") Budget budget,
          @Param("categorygroup")CategoryGroup categoryGroup
  );

  /**
   * Delete specified CategoryGroup from current Budget.
   * @param budget Budget from which CategoryGroup will be removed
   * @param categoryGroup CategoryGroup that have to be removed from specified Budget
   */
  @Modifying
  @Query(value = "DELETE FROM Budgetcategorygroup "
               + "WHERE budget_id = :budget AND category_group_id = :categorygroup",
         nativeQuery = true)
  void removeCategoryGroupFromBudget(
          @Param("budget") Budget budget,
          @Param("categorygroup")CategoryGroup categoryGroup
  );

  /**
   * Add Category to specified Budget.
   * @param budget Budget to which Category will be added
   * @param category Category that have to be added to specified Budget
   */
  @Modifying
  @Query(value = "INSERT INTO Budgetcategory (budget_id, category_id) "
               + "VALUES (:budget, :category)",
         nativeQuery = true)
  void addCategoryToBudget(
          @Param("budget") Budget budget,
          @Param("category")Category category
  );

  /**
   * Delete specified Category from current Budget.
   * @param budget Budget from which Category will be removed
   * @param category Category that have to be removed from specified Budget
   */
  @Modifying
  @Query(value = "DELETE FROM Budgetcategory "
               + "WHERE budget_id = :budget AND category_id = :category",
         nativeQuery = true)
  void removeCategoryFromBudget(
          @Param("budget") Budget budget,
          @Param("category")Category category
  );
}
